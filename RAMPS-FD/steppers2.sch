EESchema Schematic File Version 4
LIBS:RAMPS-FD-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 10
Title "RAMPS-FD (RAMPS for Arduino Due)"
Date "2016-07-17"
Rev "v2.2"
Comp ""
Comment1 "Derived from RAMPS 1.4 reprap.org/wiki/RAMPS1.4"
Comment2 "GPL v3"
Comment3 "Bob Cousins 2016"
Comment4 ""
$EndDescr
Text GLabel 3350 6300 0    50   Input ~ 0
/E2_EN_BUF
Text GLabel 3450 3850 0    50   Input ~ 0
/E1_EN_BUF
Wire Wire Line
	6100 6200 6100 6500
Wire Wire Line
	6100 6800 6100 7200
Wire Wire Line
	5450 6000 5450 6200
Wire Wire Line
	5200 6200 5450 6200
Wire Wire Line
	5500 6700 5200 6700
Wire Wire Line
	5500 6500 5200 6500
Wire Wire Line
	6100 7200 5200 7200
Wire Wire Line
	5200 7100 5500 7100
Wire Wire Line
	5200 6600 5500 6600
Wire Wire Line
	5200 6800 5500 6800
Wire Wire Line
	2450 5600 3550 5600
Wire Wire Line
	3800 5600 3800 6200
Wire Wire Line
	3800 6200 3900 6200
Connection ~ 3800 7100
Wire Wire Line
	3800 7100 3800 7200
Wire Wire Line
	3800 7200 3900 7200
Connection ~ 2450 6700
Wire Wire Line
	2450 6700 2500 6700
Wire Wire Line
	3850 6500 3900 6500
Wire Wire Line
	3300 6800 3900 6800
Connection ~ 3550 5600
Wire Wire Line
	3550 5550 3550 5600
Wire Wire Line
	3300 6700 3450 6700
Wire Wire Line
	3900 6900 3300 6900
Wire Wire Line
	3900 6600 3850 6600
Wire Wire Line
	2500 6800 2450 6800
Wire Wire Line
	2450 6900 2500 6900
Wire Wire Line
	2450 5600 2450 6700
Connection ~ 2450 6800
Wire Wire Line
	3600 7100 3800 7100
Connection ~ 5450 6200
Wire Wire Line
	3450 7100 3450 6700
Wire Wire Line
	5500 7550 3450 7550
Wire Wire Line
	3450 7550 3450 7400
Wire Wire Line
	5500 7100 5500 7550
Connection ~ 5500 7550
Connection ~ 3450 6700
Connection ~ 3550 4250
Connection ~ 3650 1850
Connection ~ 5600 5100
Wire Wire Line
	5600 4650 5600 5100
Wire Wire Line
	3550 4950 3550 5100
Wire Wire Line
	3550 5100 5600 5100
Wire Wire Line
	3550 4650 3550 4250
Connection ~ 5700 2700
Wire Wire Line
	5700 2250 5700 2700
Connection ~ 5650 1350
Connection ~ 5550 3750
Wire Wire Line
	3700 4650 3900 4650
Connection ~ 2550 4350
Wire Wire Line
	2550 3150 2550 4250
Wire Wire Line
	2550 4450 2600 4450
Wire Wire Line
	2550 4350 2600 4350
Wire Wire Line
	4000 4150 3950 4150
Wire Wire Line
	4000 4450 3400 4450
Wire Wire Line
	3400 4250 3550 4250
Wire Wire Line
	2550 3150 3650 3150
Wire Wire Line
	3650 3150 3650 3100
Connection ~ 3650 3150
Wire Wire Line
	3400 4350 4000 4350
Wire Wire Line
	3950 4050 4000 4050
Wire Wire Line
	2600 4250 2550 4250
Connection ~ 2550 4250
Wire Wire Line
	4000 4750 3900 4750
Wire Wire Line
	3900 4750 3900 4650
Connection ~ 3900 4650
Wire Wire Line
	4000 3750 3900 3750
Wire Wire Line
	3900 3750 3900 3150
Wire Wire Line
	2550 750  3750 750 
Wire Wire Line
	4000 750  4000 1350
Wire Wire Line
	4000 1350 4100 1350
Connection ~ 4000 2250
Wire Wire Line
	4000 2250 4000 2350
Wire Wire Line
	4000 2350 4100 2350
Connection ~ 2550 1850
Wire Wire Line
	2550 1850 2600 1850
Wire Wire Line
	4050 1650 4100 1650
Wire Wire Line
	3400 1950 4100 1950
Connection ~ 3750 750 
Wire Wire Line
	3750 750  3750 700 
Wire Wire Line
	5650 1150 5650 1350
Wire Wire Line
	5400 1350 5650 1350
Wire Wire Line
	5700 1850 5400 1850
Wire Wire Line
	5700 1650 5400 1650
Wire Wire Line
	5400 2350 6300 2350
Wire Wire Line
	5700 2250 5400 2250
Wire Wire Line
	5400 1750 5700 1750
Wire Wire Line
	5400 1950 5700 1950
Wire Wire Line
	3400 1850 3650 1850
Wire Wire Line
	4100 2050 3400 2050
Wire Wire Line
	4100 1750 4050 1750
Wire Wire Line
	2600 1950 2550 1950
Wire Wire Line
	2550 2050 2600 2050
Wire Wire Line
	2550 750  2550 1850
Connection ~ 2550 1950
Wire Wire Line
	3800 2250 4000 2250
Wire Wire Line
	5300 4350 5600 4350
Wire Wire Line
	5300 4150 5600 4150
Wire Wire Line
	5600 4650 5300 4650
Wire Wire Line
	5300 4750 6200 4750
Wire Wire Line
	5600 4050 5300 4050
Wire Wire Line
	5600 4250 5300 4250
Wire Wire Line
	5300 3750 5550 3750
Wire Wire Line
	5550 3550 5550 3750
Wire Wire Line
	6300 1350 6300 1650
Wire Wire Line
	6300 2350 6300 1950
Wire Wire Line
	6200 4750 6200 4350
Wire Wire Line
	6200 3750 6200 4050
Wire Wire Line
	3650 2250 3650 1850
Wire Wire Line
	5700 2700 3650 2700
Wire Wire Line
	3650 2700 3650 2550
Text GLabel 3300 1450 0    50   Input ~ 0
/E0_EN_BUF
$Comp
L RAMPS-FD-rescue:CONN_3X2 P209
U 1 1 51B4E8E0
P 3000 4400
F 0 "P209" H 3000 4650 50  0000 C CNN
F 1 "3 2 1" V 3000 4450 40  0000 C CNN
F 2 "pin_array:pin_array_3x2" H 3000 4400 60  0001 C CNN
F 3 "https://s3.amazonaws.com/catalogspreads-pdf/PAGE110-111%20.100%20MALE%20HDR%20ST.pdf" H 3000 4400 60  0001 C CNN
F 4 "CONN HEADER .100\" DUAL STR 6POS" H 0   0   50  0001 C CNN "Description"
F 5 "Sullins Connector Solutions" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "PRPC003DAAN-RC" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "S2011EC-03-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/products/en?keywords=PRPC003DAAN-RC" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    3000 4400
	1    0    0    -1  
$EndComp
Text GLabel 3950 4050 0    50   Input ~ 0
E1_STEP
Text GLabel 3950 4150 0    50   Input ~ 0
E1_DIR
$Comp
L power:GND #PWR059
U 1 1 51B4E8DF
P 5600 5250
AR Path="/51B4E8DF" Ref="#PWR059"  Part="1" 
AR Path="/51B4E84F/51B4E8DF" Ref="#PWR060"  Part="1" 
F 0 "#PWR060" H 5600 5250 30  0001 C CNN
F 1 "GND" H 5600 5180 30  0001 C CNN
F 2 "" H 5600 5250 60  0001 C CNN
F 3 "" H 5600 5250 60  0001 C CNN
	1    5600 5250
	1    0    0    -1  
$EndComp
$Comp
L RAMPS-FD-rescue:CONN_4 P212
U 1 1 51B4E8DE
P 5950 4200
F 0 "P212" V 5900 4200 50  0000 C CNN
F 1 "E1" V 6000 4200 50  0000 C CNN
F 2 "rmc:CONN_KK_2.54_4W" H 5950 4200 60  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/ps/PS-10-07-001.pdf" H 5950 4200 60  0001 C CNN
F 4 "CONN HEADER 4POS .100 VERT TIN" H 0   0   50  0001 C CNN "Description"
F 5 "Molex, LLC" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "0022232041" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 8 "WM4202-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 9 "https://www.digikey.com/product-detail/en/molex-llc/0022232041/WM4202-ND/26671" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 10 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5950 4200
	1    0    0    -1  
$EndComp
$Comp
L RAMPS-FD-rescue:CONN_4 P217
U 1 1 51B4E8DD
P 6050 1800
F 0 "P217" V 6000 1800 50  0000 C CNN
F 1 "E0" V 6100 1800 50  0000 C CNN
F 2 "rmc:CONN_KK_2.54_4W" H 6050 1800 60  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/ps/PS-10-07-001.pdf" H 6050 1800 60  0001 C CNN
F 4 "CONN HEADER 4POS .100 VERT TIN" H 0   0   50  0001 C CNN "Description"
F 5 "Molex, LLC" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "0022232041" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 8 "WM4202-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 9 "https://www.digikey.com/product-detail/en/molex-llc/0022232041/WM4202-ND/26671" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 10 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    6050 1800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR060
U 1 1 51B4E8DC
P 5700 2800
AR Path="/51B4E8DC" Ref="#PWR060"  Part="1" 
AR Path="/51B4E84F/51B4E8DC" Ref="#PWR061"  Part="1" 
F 0 "#PWR061" H 5700 2800 30  0001 C CNN
F 1 "GND" H 5700 2730 30  0001 C CNN
F 2 "" H 5700 2800 60  0001 C CNN
F 3 "" H 5700 2800 60  0001 C CNN
	1    5700 2800
	1    0    0    -1  
$EndComp
Text GLabel 4050 1750 0    50   Input ~ 0
E0_DIR
Text GLabel 4050 1650 0    50   Input ~ 0
E0_STEP
$Comp
L RAMPS-FD-rescue:CONN_3X2 P208
U 1 1 51B4E8DB
P 3000 2000
F 0 "P208" H 3000 2250 50  0000 C CNN
F 1 "3 2 1" V 3000 2050 40  0000 C CNN
F 2 "pin_array:pin_array_3x2" H 3000 2000 60  0001 C CNN
F 3 "https://s3.amazonaws.com/catalogspreads-pdf/PAGE110-111%20.100%20MALE%20HDR%20ST.pdf" H 3000 2000 60  0001 C CNN
F 4 "CONN HEADER .100\" DUAL STR 6POS" H 0   0   50  0001 C CNN "Description"
F 5 "Sullins Connector Solutions" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "PRPC003DAAN-RC" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "S2011EC-03-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/products/en?keywords=PRPC003DAAN-RC" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    3000 2000
	1    0    0    -1  
$EndComp
$Comp
L RMC:POLOLU U205
U 1 1 51B4E8D9
P 4650 4250
F 0 "U205" H 4350 4850 60  0000 C CNN
F 1 "E1" H 4450 3600 60  0000 C CNN
F 2 "rmc:POLOLU" H 4650 4250 60  0001 C CNN
F 3 "https://s3.amazonaws.com/catalogspreads-pdf/PAGE114-115%20.100%20FEMALE%20HDR.pdf" H 4650 4250 60  0001 C CNN
F 4 "CONN HEADER FEMALE 8POS .1\" TIN" H 0   0   50  0001 C CNN "Description"
F 5 "Sullins Connector Solutions" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "PPTC081LFBN-RC" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "2" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "S7006-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/products/en?keywords=PPTC081LFBN-RC" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    4650 4250
	1    0    0    -1  
$EndComp
$Comp
L RMC:POLOLU U206
U 1 1 51B4E8D8
P 4750 1850
F 0 "U206" H 4450 2450 60  0000 C CNN
F 1 "E0" H 4550 1200 60  0000 C CNN
F 2 "rmc:POLOLU" H 4750 1850 60  0001 C CNN
F 3 "https://s3.amazonaws.com/catalogspreads-pdf/PAGE114-115%20.100%20FEMALE%20HDR.pdf" H 4750 1850 60  0001 C CNN
F 4 "CONN HEADER FEMALE 8POS .1\" TIN" H 0   0   50  0001 C CNN "Description"
F 5 "Sullins Connector Solutions" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "PPTC081LFBN-RC" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "2" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "S7006-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/products/en?keywords=PPTC081LFBN-RC" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    4750 1850
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C206
U 1 1 51B4E8D7
P 6300 1800
AR Path="/51B4E8D7" Ref="C206"  Part="1" 
AR Path="/51B4E84F/51B4E8D7" Ref="C206"  Part="1" 
F 0 "C206" H 6350 1900 50  0000 L CNN
F 1 "100u 35V" H 6350 1700 50  0000 L CNN
F 2 "Capacitors_SMD:c_elec_6.3x7.7" H 6300 1800 60  0001 C CNN
F 3 "http://nichicon-us.com/english/products/pdfs/e-ucd.pdf" H 6300 1800 60  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Characteristics"
F 5 "Nichicon" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "UCD1V101MCL6GS" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 8 "493-6420-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 9 "https://www.digikey.com/product-detail/en/nichicon/UCD1V101MCL6GS/493-6420-1-ND/3438934" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 10 "CAP ALUM 100UF 20% 35V SMD" H 0   0   50  0001 C CNN "Description"
F 11 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    6300 1800
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C205
U 1 1 51B4E8D6
P 6200 4200
AR Path="/51B4E8D6" Ref="C205"  Part="1" 
AR Path="/51B4E84F/51B4E8D6" Ref="C205"  Part="1" 
F 0 "C205" H 6250 4300 50  0000 L CNN
F 1 "100u 35V" H 6250 4100 50  0000 L CNN
F 2 "Capacitors_SMD:c_elec_6.3x7.7" H 6200 4200 60  0001 C CNN
F 3 "http://nichicon-us.com/english/products/pdfs/e-ucd.pdf" H 6200 4200 60  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Characteristics"
F 5 "Nichicon" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "UCD1V101MCL6GS" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 8 "493-6420-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 9 "https://www.digikey.com/product-detail/en/nichicon/UCD1V101MCL6GS/493-6420-1-ND/3438934" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 10 "CAP ALUM 100UF 20% 35V SMD" H 0   0   50  0001 C CNN "Description"
F 11 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    6200 4200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R210
U 1 1 51B4E8D5
P 3650 2400
AR Path="/51B4E8D5" Ref="R210"  Part="1" 
AR Path="/51B4E84F/51B4E8D5" Ref="R210"  Part="1" 
F 0 "R210" V 3730 2400 50  0000 C CNN
F 1 "100k" V 3650 2400 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 3650 2400 60  0001 C CNN
F 3 "http://industrial.panasonic.com/www-cgi/jvcr13pz.cgi?E+PZ+3+AOA0001+ERJ3GEYJ104V+7+WW" H 3650 2400 60  0001 C CNN
F 4 "RES SMD 100K OHM 5% 1/10W 0603" H 0   0   50  0001 C CNN "Description"
F 5 "‎Panasonic Electronic Components‎" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "ERJ-3GEYJ104V" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "P100KGCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/panasonic-electronic-components/ERJ-3GEYJ104V/P100KGCT-ND/134878" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    3650 2400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R208
U 1 1 51B4E8D4
P 3550 4800
AR Path="/51B4E8D4" Ref="R208"  Part="1" 
AR Path="/51B4E84F/51B4E8D4" Ref="R208"  Part="1" 
F 0 "R208" V 3630 4800 50  0000 C CNN
F 1 "100k" V 3550 4800 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 3550 4800 60  0001 C CNN
F 3 "http://industrial.panasonic.com/www-cgi/jvcr13pz.cgi?E+PZ+3+AOA0001+ERJ3GEYJ104V+7+WW" H 3550 4800 60  0001 C CNN
F 4 "RES SMD 100K OHM 5% 1/10W 0603" H 0   0   50  0001 C CNN "Description"
F 5 "‎Panasonic Electronic Components‎" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "ERJ-3GEYJ104V" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "P100KGCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/panasonic-electronic-components/ERJ-3GEYJ104V/P100KGCT-ND/134878" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    3550 4800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R207
U 1 1 51B4E8D3
P 3450 7250
AR Path="/51B4E8D3" Ref="R207"  Part="1" 
AR Path="/51B4E84F/51B4E8D3" Ref="R207"  Part="1" 
F 0 "R207" V 3530 7250 50  0000 C CNN
F 1 "100k" V 3450 7250 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 3450 7250 60  0001 C CNN
F 3 "http://industrial.panasonic.com/www-cgi/jvcr13pz.cgi?E+PZ+3+AOA0001+ERJ3GEYJ104V+7+WW" H 3450 7250 60  0001 C CNN
F 4 "RES SMD 100K OHM 5% 1/10W 0603" H 0   0   50  0001 C CNN "Description"
F 5 "‎Panasonic Electronic Components‎" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "ERJ-3GEYJ104V" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "P100KGCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/panasonic-electronic-components/ERJ-3GEYJ104V/P100KGCT-ND/134878" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    3450 7250
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C204
U 1 1 51B4E8D2
P 6100 6650
AR Path="/51B4E8D2" Ref="C204"  Part="1" 
AR Path="/51B4E84F/51B4E8D2" Ref="C204"  Part="1" 
F 0 "C204" H 6150 6750 50  0000 L CNN
F 1 "100u 35V" H 6150 6550 50  0000 L CNN
F 2 "Capacitors_SMD:c_elec_6.3x7.7" H 6100 6650 60  0001 C CNN
F 3 "http://nichicon-us.com/english/products/pdfs/e-ucd.pdf" H 6100 6650 60  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Characteristics"
F 5 "Nichicon" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "UCD1V101MCL6GS" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 8 "493-6420-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 9 "https://www.digikey.com/product-detail/en/nichicon/UCD1V101MCL6GS/493-6420-1-ND/3438934" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 10 "CAP ALUM 100UF 20% 35V SMD" H 0   0   50  0001 C CNN "Description"
F 11 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    6100 6650
	1    0    0    -1  
$EndComp
$Comp
L RMC:POLOLU U204
U 1 1 51B4E8D1
P 4550 6700
F 0 "U204" H 4250 7300 60  0000 C CNN
F 1 "E2" H 4350 6050 60  0000 C CNN
F 2 "rmc:POLOLU" H 4550 6700 60  0001 C CNN
F 3 "https://s3.amazonaws.com/catalogspreads-pdf/PAGE114-115%20.100%20FEMALE%20HDR.pdf" H 4550 6700 60  0001 C CNN
F 4 "CONN HEADER FEMALE 8POS .1\" TIN" H 0   0   50  0001 C CNN "Description"
F 5 "Sullins Connector Solutions" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "PPTC081LFBN-RC" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "2" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "S7006-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/products/en?keywords=PPTC081LFBN-RC" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    4550 6700
	1    0    0    -1  
$EndComp
$Comp
L RAMPS-FD-rescue:CONN_4 P210
U 1 1 51B4E8D0
P 5850 6650
F 0 "P210" V 5800 6650 50  0000 C CNN
F 1 "E2" V 5900 6650 50  0000 C CNN
F 2 "rmc:CONN_KK_2.54_4W" H 5850 6650 60  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/ps/PS-10-07-001.pdf" H 5850 6650 60  0001 C CNN
F 4 "CONN HEADER 4POS .100 VERT TIN" H 0   0   50  0001 C CNN "Description"
F 5 "Molex, LLC" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "0022232041" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 8 "WM4202-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 9 "https://www.digikey.com/product-detail/en/molex-llc/0022232041/WM4202-ND/26671" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 10 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5850 6650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR061
U 1 1 51B4E8CF
P 5500 7700
AR Path="/51B4E8CF" Ref="#PWR061"  Part="1" 
AR Path="/51B4E84F/51B4E8CF" Ref="#PWR062"  Part="1" 
F 0 "#PWR062" H 5500 7700 30  0001 C CNN
F 1 "GND" H 5500 7630 30  0001 C CNN
F 2 "" H 5500 7700 60  0001 C CNN
F 3 "" H 5500 7700 60  0001 C CNN
	1    5500 7700
	1    0    0    -1  
$EndComp
Text GLabel 3850 6600 0    50   Input ~ 0
E2_DIR
Text GLabel 3850 6500 0    50   Input ~ 0
E2_STEP
$Comp
L RAMPS-FD-rescue:CONN_3X2 P207
U 1 1 51B4E8CE
P 2900 6850
F 0 "P207" H 2900 7100 50  0000 C CNN
F 1 "3 2 1" V 2900 6900 40  0000 C CNN
F 2 "pin_array:pin_array_3x2" H 2900 6850 60  0001 C CNN
F 3 "https://s3.amazonaws.com/catalogspreads-pdf/PAGE110-111%20.100%20MALE%20HDR%20ST.pdf" H 2900 6850 60  0001 C CNN
F 4 "CONN HEADER .100\" DUAL STR 6POS" H 0   0   50  0001 C CNN "Description"
F 5 "Sullins Connector Solutions" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "PRPC003DAAN-RC" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "S2011EC-03-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/products/en?keywords=PRPC003DAAN-RC" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    2900 6850
	1    0    0    -1  
$EndComp
$Comp
L RMC:+V_MOTOR #PWR063
U 1 1 51B4E8CC
P 5450 6000
F 0 "#PWR063" H 5450 5970 30  0001 C CNN
F 1 "+V_MOTOR" H 5450 6100 30  0000 C CNN
F 2 "" H 5450 6000 60  0001 C CNN
F 3 "" H 5450 6000 60  0001 C CNN
	1    5450 6000
	1    0    0    -1  
$EndComp
$Comp
L RMC:+V_MOTOR #PWR064
U 1 1 51B4E8CB
P 5550 3550
F 0 "#PWR064" H 5550 3520 30  0001 C CNN
F 1 "+V_MOTOR" H 5550 3650 30  0000 C CNN
F 2 "" H 5550 3550 60  0001 C CNN
F 3 "" H 5550 3550 60  0001 C CNN
	1    5550 3550
	1    0    0    -1  
$EndComp
$Comp
L RMC:+V_MOTOR #PWR065
U 1 1 51B4E8CA
P 5650 1150
F 0 "#PWR065" H 5650 1120 30  0001 C CNN
F 1 "+V_MOTOR" H 5650 1250 30  0000 C CNN
F 2 "" H 5650 1150 60  0001 C CNN
F 3 "" H 5650 1150 60  0001 C CNN
	1    5650 1150
	1    0    0    -1  
$EndComp
$Comp
L RMC:+V_LOGIC #PWR066
U 1 1 51B4E8C9
P 3550 5550
F 0 "#PWR066" H 3550 5520 30  0001 C CNN
F 1 "+V_LOGIC" H 3550 5650 30  0000 C CNN
F 2 "" H 3550 5550 60  0001 C CNN
F 3 "" H 3550 5550 60  0001 C CNN
	1    3550 5550
	1    0    0    -1  
$EndComp
$Comp
L RMC:+V_LOGIC #PWR067
U 1 1 51B4E8C8
P 3650 3100
F 0 "#PWR067" H 3650 3070 30  0001 C CNN
F 1 "+V_LOGIC" H 3650 3200 30  0000 C CNN
F 2 "" H 3650 3100 60  0001 C CNN
F 3 "" H 3650 3100 60  0001 C CNN
	1    3650 3100
	1    0    0    -1  
$EndComp
$Comp
L RMC:+V_LOGIC #PWR068
U 1 1 51B4E8C7
P 3750 700
F 0 "#PWR068" H 3750 670 30  0001 C CNN
F 1 "+V_LOGIC" H 3750 800 30  0000 C CNN
F 2 "" H 3750 700 60  0001 C CNN
F 3 "" H 3750 700 60  0001 C CNN
	1    3750 700 
	1    0    0    -1  
$EndComp
$Comp
L RMC:+V_LOGIC #PWR069
U 1 1 51B4E8C6
P 3800 2250
F 0 "#PWR069" H 3800 2220 30  0001 C CNN
F 1 "+V_LOGIC" H 3800 2350 30  0000 C CNN
F 2 "" H 3800 2250 60  0001 C CNN
F 3 "" H 3800 2250 60  0001 C CNN
	1    3800 2250
	1    0    0    -1  
$EndComp
$Comp
L RMC:+V_LOGIC #PWR070
U 1 1 51B4E8C5
P 3600 7100
F 0 "#PWR070" H 3600 7070 30  0001 C CNN
F 1 "+V_LOGIC" H 3600 7200 30  0000 C CNN
F 2 "" H 3600 7100 60  0001 C CNN
F 3 "" H 3600 7100 60  0001 C CNN
	1    3600 7100
	1    0    0    -1  
$EndComp
$Comp
L RMC:+V_LOGIC #PWR071
U 1 1 51B4E8C4
P 3700 4650
F 0 "#PWR071" H 3700 4620 30  0001 C CNN
F 1 "+V_LOGIC" H 3700 4750 30  0000 C CNN
F 2 "" H 3700 4650 60  0001 C CNN
F 3 "" H 3700 4650 60  0001 C CNN
	1    3700 4650
	1    0    0    -1  
$EndComp
Text GLabel 5650 7200 2    50   UnSpc ~ 0
MOT_GND
Text GLabel 5750 4750 2    50   UnSpc ~ 0
MOT_GND
Text GLabel 5850 2350 2    50   UnSpc ~ 0
MOT_GND
Text Label 5250 6800 0    40   ~ 0
E2-1B
Text Label 5250 6700 0    40   ~ 0
E2-1A
Text Label 5250 6600 0    40   ~ 0
E2-2A
Text Label 5250 6500 0    40   ~ 0
E2-2B
Text Label 5350 4050 0    40   ~ 0
E1-2B
Text Label 5350 4150 0    40   ~ 0
E1-2A
Text Label 5350 4250 0    40   ~ 0
E1-1A
Text Label 5350 4350 0    40   ~ 0
E1-1B
Text Label 5450 1650 0    40   ~ 0
E0-2B
Text Label 5450 1750 0    40   ~ 0
E0-2A
Text Label 5450 1850 0    40   ~ 0
E0-1A
Text Label 5450 1950 0    40   ~ 0
E0-1B
Wire Wire Line
	3800 7100 3900 7100
Wire Wire Line
	2450 6700 2450 6800
Wire Wire Line
	3550 5600 3800 5600
Wire Wire Line
	2450 6800 2450 6900
Wire Wire Line
	5450 6200 6100 6200
Wire Wire Line
	5500 7550 5500 7700
Wire Wire Line
	3450 6700 3900 6700
Wire Wire Line
	3550 4250 4000 4250
Wire Wire Line
	3650 1850 4100 1850
Wire Wire Line
	5600 5100 5600 5250
Wire Wire Line
	5700 2700 5700 2800
Wire Wire Line
	5650 1350 6300 1350
Wire Wire Line
	5550 3750 6200 3750
Wire Wire Line
	2550 4350 2550 4450
Wire Wire Line
	3650 3150 3900 3150
Wire Wire Line
	2550 4250 2550 4350
Wire Wire Line
	3900 4650 4000 4650
Wire Wire Line
	4000 2250 4100 2250
Wire Wire Line
	2550 1850 2550 1950
Wire Wire Line
	3750 750  4000 750 
Wire Wire Line
	2550 1950 2550 2050
Wire Wire Line
	3300 1450 4100 1450
Wire Wire Line
	3350 6300 3900 6300
Wire Wire Line
	3450 3850 4000 3850
$EndSCHEMATC
