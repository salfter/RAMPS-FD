EESchema Schematic File Version 4
LIBS:RAMPS-FD-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 10
Title "RAMPS-FD (RAMPS for Arduino Due)"
Date "2016-07-17"
Rev "v2.2"
Comp ""
Comment1 "Derived from RAMPS 1.4 reprap.org/wiki/RAMPS1.4"
Comment2 "GPL v3"
Comment3 "Bob Cousins 2016"
Comment4 ""
$EndDescr
$Comp
L power:VCC #PWR047
U 1 1 523A3B4A
P 6200 1200
F 0 "#PWR047" H 6200 1050 50  0001 C CNN
F 1 "VCC" H 6200 1350 50  0000 C CNN
F 2 "" H 6200 1200 50  0000 C CNN
F 3 "" H 6200 1200 50  0000 C CNN
	1    6200 1200
	1    0    0    -1  
$EndComp
$Comp
L RAMPS-FD-rescue:R_PACK4 RP1
U 1 1 523A3B30
P 6450 6900
F 0 "RP1" H 6450 7350 40  0000 C CNN
F 1 "10k" H 6450 6850 40  0000 C CNN
F 2 "Resistors_SMD:R_Cat16-4" H 6450 6900 60  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Yageo%20PDFs/YC102-358,TC122-164_Series_DS.pdf" H 6450 6900 60  0001 C CNN
F 4 "RES ARRAY 4 RES 10K OHM 1206" H 0   0   50  0001 C CNN "Description"
F 5 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "YC164-JR-0710KL" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "YC164J-10KCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/yageo/YC164-JR-0710KL/YC164J-10KCT-ND/1005702" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "1206" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    6450 6900
	0    -1   -1   0   
$EndComp
$Comp
L RAMPS-FD-rescue:R_PACK4 RP3
U 1 1 523A3B2E
P 7250 1700
F 0 "RP3" H 7250 2150 40  0000 C CNN
F 1 "10k" H 7250 1650 40  0000 C CNN
F 2 "Resistors_SMD:R_Cat16-4" H 7250 1700 60  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Yageo%20PDFs/YC102-358,TC122-164_Series_DS.pdf" H 7250 1700 60  0001 C CNN
F 4 "RES ARRAY 4 RES 10K OHM 1206" H 0   0   50  0001 C CNN "Description"
F 5 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "YC164-JR-0710KL" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "YC164J-10KCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/yageo/YC164-JR-0710KL/YC164J-10KCT-ND/1005702" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "1206" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    7250 1700
	0    -1   -1   0   
$EndComp
$Comp
L RAMPS-FD-rescue:R_PACK4 RP2
U 1 1 523A3B27
P 6550 1700
F 0 "RP2" H 6550 2150 40  0000 C CNN
F 1 "10k" H 6550 1650 40  0000 C CNN
F 2 "Resistors_SMD:R_Cat16-4" H 6550 1700 60  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Yageo%20PDFs/YC102-358,TC122-164_Series_DS.pdf" H 6550 1700 60  0001 C CNN
F 4 "RES ARRAY 4 RES 10K OHM 1206" H 0   0   50  0001 C CNN "Description"
F 5 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "YC164-JR-0710KL" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "YC164J-10KCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/yageo/YC164-JR-0710KL/YC164J-10KCT-ND/1005702" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "1206" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    6550 1700
	0    -1   -1   0   
$EndComp
Text Notes 525  825  0    60   ~ 0
Emergency Stop switch (Normally Closed type)\nuse jumper to bypass\n\n
Text GLabel 7650 5100 2    60   Output ~ 0
FET4_BUF
Text GLabel 7650 4350 2    60   Output ~ 0
D10_BUF
$Comp
L power:GND #PWR049
U 1 1 523A374A
P 3700 7600
AR Path="/523A374A" Ref="#PWR049"  Part="1" 
AR Path="/5239FA54/523A374A" Ref="#PWR048"  Part="1" 
F 0 "#PWR048" H 3700 7600 30  0001 C CNN
F 1 "GND" H 3700 7530 30  0001 C CNN
F 2 "" H 3700 7600 60  0001 C CNN
F 3 "" H 3700 7600 60  0001 C CNN
	1    3700 7600
	1    0    0    -1  
$EndComp
Text GLabel 3050 5100 0    60   Input ~ 0
D11-FET4
Text GLabel 3000 4350 0    60   Input ~ 0
D10
$Comp
L RAMPS-FD-rescue:74LS125 U1
U 1 1 523A36ED
P 4850 6450
F 0 "U1" H 4850 6550 50  0000 L BNN
F 1 "74ACT125" H 4900 6300 40  0000 L TNN
F 2 "Housings_SSOP:TSSOP-14_4.4x5mm_Pitch0.65mm" H 4850 6450 60  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/MC74AC125-D.PDF" H 4850 6450 60  0001 C CNN
F 4 "IC BUF NON-INVERT 5.5V 14TSSOP" H 0   0   50  0001 C CNN "Description"
F 5 "14-TSSOP" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "ON Semiconductor" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "MC74ACT125DTR2G" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "MC74ACT125DTR2GOSCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/product-detail/en/on-semiconductor/MC74ACT125DTR2G/MC74ACT125DTR2GOSCT-ND/3462326" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    4850 6450
	1    0    0    1   
$EndComp
$Comp
L RAMPS-FD-rescue:74LS125 U1
U 4 1 523A36EC
P 4900 5700
F 0 "U1" H 4900 5800 50  0000 L BNN
F 1 "74ACT125" H 4950 5550 40  0000 L TNN
F 2 "Housings_SSOP:TSSOP-14_4.4x5mm_Pitch0.65mm" H 4900 5700 60  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/MC74AC125-D.PDF" H 4900 5700 60  0001 C CNN
F 4 "IC BUF NON-INVERT 5.5V 14TSSOP" H 0   0   50  0001 C CNN "Description"
F 5 "14-TSSOP" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "ON Semiconductor" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "MC74ACT125DTR2G" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "MC74ACT125DTR2GOSCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/product-detail/en/on-semiconductor/MC74ACT125DTR2G/MC74ACT125DTR2GOSCT-ND/3462326" H 0   0   50  0001 C CNN "Vendor 1 URL"
	4    4900 5700
	1    0    0    1   
$EndComp
$Comp
L RAMPS-FD-rescue:74LS125 U1
U 3 1 523A36E7
P 4900 5100
F 0 "U1" H 4900 5200 50  0000 L BNN
F 1 "74ACT125" H 4950 4950 40  0000 L TNN
F 2 "Housings_SSOP:TSSOP-14_4.4x5mm_Pitch0.65mm" H 4900 5100 60  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/MC74AC125-D.PDF" H 4900 5100 60  0001 C CNN
F 4 "IC BUF NON-INVERT 5.5V 14TSSOP" H 0   0   50  0001 C CNN "Description"
F 5 "14-TSSOP" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "ON Semiconductor" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "MC74ACT125DTR2G" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "MC74ACT125DTR2GOSCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/product-detail/en/on-semiconductor/MC74ACT125DTR2G/MC74ACT125DTR2GOSCT-ND/3462326" H 0   0   50  0001 C CNN "Vendor 1 URL"
	3    4900 5100
	1    0    0    1   
$EndComp
$Comp
L RAMPS-FD-rescue:74LS125 U1
U 2 1 523A36E3
P 4900 4350
F 0 "U1" H 4900 4450 50  0000 L BNN
F 1 "74ACT125" H 4950 4200 40  0000 L TNN
F 2 "Housings_SSOP:TSSOP-14_4.4x5mm_Pitch0.65mm" H 4900 4350 60  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/MC74AC125-D.PDF" H 4900 4350 60  0001 C CNN
F 4 "IC BUF NON-INVERT 5.5V 14TSSOP" H 0   0   50  0001 C CNN "Description"
F 5 "14-TSSOP" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "ON Semiconductor" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "MC74ACT125DTR2G" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "MC74ACT125DTR2GOSCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/product-detail/en/on-semiconductor/MC74ACT125DTR2G/MC74ACT125DTR2GOSCT-ND/3462326" H 0   0   50  0001 C CNN "Vendor 1 URL"
	2    4900 4350
	1    0    0    1   
$EndComp
$Comp
L RAMPS-FD-rescue:74LS244 U2
U 1 1 523A33CC
P 5100 2800
F 0 "U2" H 5150 2600 60  0000 C CNN
F 1 "74ACT244" H 5200 2400 60  0000 C CNN
F 2 "Housings_SSOP:TSSOP-20_4.4x6.5mm_Pitch0.65mm" H 5100 2800 60  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn74act244.pdf" H 5100 2800 60  0001 C CNN
F 4 "IC BUF NON-INVERT 5.5V 20TSSOP" H 0   0   50  0001 C CNN "Description"
F 5 "20-TSSOP" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "Texas Instruments" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "SN74ACT244PWR" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "296-1071-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/product-detail/en/texas-instruments/SN74ACT244PWR/296-1071-1-ND/276339" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    5100 2800
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR049
U 1 1 51E4F650
P 10650 900
F 0 "#PWR049" H 10650 750 50  0001 C CNN
F 1 "VCC" H 10650 1050 50  0000 C CNN
F 2 "" H 10650 900 50  0000 C CNN
F 3 "" H 10650 900 50  0000 C CNN
	1    10650 900 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR051
U 1 1 51E4F64F
P 10650 1700
AR Path="/51E4F64F" Ref="#PWR051"  Part="1" 
AR Path="/5239FA54/51E4F64F" Ref="#PWR050"  Part="1" 
F 0 "#PWR050" H 10650 1700 30  0001 C CNN
F 1 "GND" H 10650 1630 30  0001 C CNN
F 2 "" H 10650 1700 60  0001 C CNN
F 3 "" H 10650 1700 60  0001 C CNN
	1    10650 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 51E4F64E
P 10650 1300
AR Path="/51E4F64E" Ref="C2"  Part="1" 
AR Path="/5239FA54/51E4F64E" Ref="C2"  Part="1" 
F 0 "C2" H 10700 1400 50  0000 L CNN
F 1 "100n" H 10700 1200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 10650 1300 60  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/CL10F104ZA8NNNC.jsp" H 10650 1300 60  0001 C CNN
F 4 "CAP CER 0.1UF 25V Y5V 0603" H 0   0   50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "CL10F104ZA8NNNC" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "1276-1011-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/products/en/capacitors/ceramic-capacitors/60?k=&pkeyword=&pv7=2&FV=380014%2C400005%2C440003%2C1f140000%2Cmu0.1%C2%B5F%7C2049%2Cffe0003c&quantity=0&ColumnSort=0&page=1&stock=1&pageSize=25" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    10650 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 51E4F640
P 10100 1300
AR Path="/51E4F640" Ref="C1"  Part="1" 
AR Path="/5239FA54/51E4F640" Ref="C1"  Part="1" 
F 0 "C1" H 10150 1400 50  0000 L CNN
F 1 "100n" H 10150 1200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 10100 1300 60  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/CL10F104ZA8NNNC.jsp" H 10100 1300 60  0001 C CNN
F 4 "CAP CER 0.1UF 25V Y5V 0603" H 0   0   50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "CL10F104ZA8NNNC" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "1276-1011-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/products/en/capacitors/ceramic-capacitors/60?k=&pkeyword=&pv7=2&FV=380014%2C400005%2C440003%2C1f140000%2Cmu0.1%C2%B5F%7C2049%2Cffe0003c&quantity=0&ColumnSort=0&page=1&stock=1&pageSize=25" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    10100 1300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR052
U 1 1 51E4F630
P 10100 1700
AR Path="/51E4F630" Ref="#PWR052"  Part="1" 
AR Path="/5239FA54/51E4F630" Ref="#PWR051"  Part="1" 
F 0 "#PWR051" H 10100 1700 30  0001 C CNN
F 1 "GND" H 10100 1630 30  0001 C CNN
F 2 "" H 10100 1700 60  0001 C CNN
F 3 "" H 10100 1700 60  0001 C CNN
	1    10100 1700
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR052
U 1 1 51E4F62B
P 10100 900
F 0 "#PWR052" H 10100 750 50  0001 C CNN
F 1 "VCC" H 10100 1050 50  0000 C CNN
F 2 "" H 10100 900 50  0000 C CNN
F 3 "" H 10100 900 50  0000 C CNN
	1    10100 900 
	1    0    0    -1  
$EndComp
Text GLabel 7600 3000 2    60   Output ~ 0
D9_BUF
Text GLabel 7600 2900 2    60   Output ~ 0
D8_BUF
Text GLabel 3150 3000 0    60   Input ~ 0
D9
Text GLabel 3150 2900 0    60   Input ~ 0
D8
$Comp
L RAMPS-FD-rescue:DIODE D301
U 1 1 51B65AB0
P 1900 1600
F 0 "D301" H 1900 1700 40  0000 C CNN
F 1 "1N4148" H 1900 1500 40  0000 C CNN
F 2 "w_smd_diode:SOD323" H 1900 1600 60  0001 C CNN
F 3 "http://www.mccsemi.com/up_pdf/1N4148WX(SOD323).pdf" H 1900 1600 60  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Characteristics"
F 5 "SOD-323" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "Micro Commercial Co" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "1N4148WX-TP" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "1N4148WXTPMSCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/micro-commercial-co/1N4148WX-TP/1N4148WXTPMSCT-ND/717312" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "DIODE GEN PURP 75V 150MA SOD323" H 0   0   50  0001 C CNN "Description"
F 12 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    1900 1600
	1    0    0    1   
$EndComp
$Comp
L Device:R R305
U 1 1 51B4ECD2
P 1300 2050
AR Path="/51B4ECD2" Ref="R305"  Part="1" 
AR Path="/5239FA54/51B4ECD2" Ref="R305"  Part="1" 
F 0 "R305" V 1380 2050 50  0000 C CNN
F 1 "4k7" V 1300 2050 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 1300 2050 60  0001 C CNN
F 3 "http://industrial.panasonic.com/www-cgi/jvcr13pz.cgi?E+PZ+3+AOA0001+ERJ3GEYJ104V+7+WW" H 1300 2050 60  0001 C CNN
F 4 "RES SMD 4.7K OHM 5% 1/10W 0603" H 0   0   50  0001 C CNN "Description"
F 5 "‎Panasonic Electronic Components‎" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "ERJ-3GEYJ472V" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "P4.7KGCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/panasonic-electronic-components/ERJ-3GEYJ472V/P4.7KGCT-ND/135199" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    1300 2050
	-1   0    0    -1  
$EndComp
$Comp
L RMC:+V_LOGIC #PWR053
U 1 1 51B4EC55
P 1550 1000
F 0 "#PWR053" H 1550 970 30  0001 C CNN
F 1 "+V_LOGIC" H 1550 1100 30  0000 C CNN
F 2 "" H 1550 1000 60  0001 C CNN
F 3 "" H 1550 1000 60  0001 C CNN
	1    1550 1000
	-1   0    0    -1  
$EndComp
$Comp
L RAMPS-FD-rescue:CONN_2 P302
U 1 1 51B4EBF7
P 800 1700
F 0 "P302" V 750 1700 40  0000 C CNN
F 1 "ESTOP" V 850 1700 40  0000 C CNN
F 2 "rmc:CONN_KK_2.54_2W" H 800 1700 60  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/ps/PS-10-07-001.pdf" H 800 1700 60  0001 C CNN
F 4 "CONN HEADER 2POS .100 VERT TIN" H 0   0   50  0001 C CNN "Description"
F 5 "Molex, LLC" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "22-23-2021" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 8 "WM4200-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 9 "https://www.digikey.com/product-detail/en/molex-llc/22-23-2021/WM4200-ND/26667" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 10 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    800  1700
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R304
U 1 1 51B4EBD5
P 1550 1250
AR Path="/51B4EBD5" Ref="R304"  Part="1" 
AR Path="/5239FA54/51B4EBD5" Ref="R304"  Part="1" 
F 0 "R304" V 1630 1250 50  0000 C CNN
F 1 "100k" V 1550 1250 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 1550 1250 60  0001 C CNN
F 3 "http://industrial.panasonic.com/www-cgi/jvcr13pz.cgi?E+PZ+3+AOA0001+ERJ3GEYJ104V+7+WW" H 1550 1250 60  0001 C CNN
F 4 "RES SMD 100K OHM 5% 1/10W 0603" H 0   0   50  0001 C CNN "Description"
F 5 "‎Panasonic Electronic Components‎" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "ERJ-3GEYJ104V" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "P100KGCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/panasonic-electronic-components/ERJ-3GEYJ104V/P100KGCT-ND/134878" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    1550 1250
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR055
U 1 1 51B4EBC7
P 1300 2350
AR Path="/51B4EBC7" Ref="#PWR055"  Part="1" 
AR Path="/5239FA54/51B4EBC7" Ref="#PWR054"  Part="1" 
F 0 "#PWR054" H 1300 2350 30  0001 C CNN
F 1 "GND" H 1300 2280 30  0001 C CNN
F 2 "" H 1300 2350 60  0001 C CNN
F 3 "" H 1300 2350 60  0001 C CNN
	1    1300 2350
	-1   0    0    -1  
$EndComp
Text GLabel 7600 2300 2    60   Output ~ 0
/X_EN_BUF
Text GLabel 7600 2400 2    60   Output ~ 0
/Y_EN_BUF
Text GLabel 7600 2500 2    60   Output ~ 0
/Z_EN_BUF
Text GLabel 7600 2700 2    60   Output ~ 0
/E1_EN_BUF
Text GLabel 7600 2600 2    60   Output ~ 0
/E0_EN_BUF
Text GLabel 7600 2800 2    60   Output ~ 0
/E2_EN_BUF
Text GLabel 2500 1600 2    50   Output ~ 0
ESTOP
Text GLabel 3150 2800 0    60   Input ~ 0
E2_EN
Text GLabel 3150 2600 0    60   Input ~ 0
E0_EN
Text GLabel 3150 2700 0    60   Input ~ 0
E1_EN
Text GLabel 3100 2500 0    60   Input ~ 0
Z_EN
Text GLabel 3100 2400 0    60   Input ~ 0
Y_EN
Text GLabel 3100 2300 0    60   Input ~ 0
X_EN
Text Notes 1750 1900 0    50   ~ 0
Or any small signal diode
Text GLabel 3000 5700 0    50   Input ~ 0
D12-FET5
Text GLabel 2950 6450 0    50   Input ~ 0
D2-FET6
Text GLabel 7600 5700 2    50   Output ~ 0
FET5_BUF
Text GLabel 7600 6450 2    50   Output ~ 0
FET6_BUF
Text Notes 8700 2550 0    80   ~ 0
Active low
Text Notes 8750 4800 0    80   ~ 0
Active high
$Comp
L Device:R R401
U 1 1 52B0D0FC
P 3400 3650
AR Path="/52B0D0FC" Ref="R401"  Part="1" 
AR Path="/5239FA54/52B0D0FC" Ref="R401"  Part="1" 
F 0 "R401" V 3480 3650 50  0000 C CNN
F 1 "10k" V 3400 3650 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 3400 3650 60  0001 C CNN
F 3 "http://industrial.panasonic.com/www-cgi/jvcr13pz.cgi?E+PZ+3+AOA0001+ERJ3GEYJ104V+7+WW" H 3400 3650 60  0001 C CNN
F 4 "RES SMD 10K OHM 5% 1/10W 0603" H 0   0   50  0001 C CNN "Description"
F 5 "‎Panasonic Electronic Components‎" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "ERJ-3GEYJ103V" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "P10KGCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/panasonic-electronic-components/ERJ-3GEYJ103V/P10KGCT-ND/134717" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    3400 3650
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R402
U 1 1 52B0D109
P 3700 3650
AR Path="/52B0D109" Ref="R402"  Part="1" 
AR Path="/5239FA54/52B0D109" Ref="R402"  Part="1" 
F 0 "R402" V 3780 3650 50  0000 C CNN
F 1 "10k" V 3700 3650 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 3700 3650 60  0001 C CNN
F 3 "http://industrial.panasonic.com/www-cgi/jvcr13pz.cgi?E+PZ+3+AOA0001+ERJ3GEYJ104V+7+WW" H 3700 3650 60  0001 C CNN
F 4 "RES SMD 10K OHM 5% 1/10W 0603" H 0   0   50  0001 C CNN "Description"
F 5 "‎Panasonic Electronic Components‎" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "ERJ-3GEYJ103V" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "P10KGCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/panasonic-electronic-components/ERJ-3GEYJ103V/P10KGCT-ND/134717" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    3700 3650
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR056
U 1 1 52B0D127
P 3700 4150
AR Path="/52B0D127" Ref="#PWR056"  Part="1" 
AR Path="/5239FA54/52B0D127" Ref="#PWR055"  Part="1" 
F 0 "#PWR055" H 3700 4150 30  0001 C CNN
F 1 "GND" H 3700 4080 30  0001 C CNN
F 2 "" H 3700 4150 60  0001 C CNN
F 3 "" H 3700 4150 60  0001 C CNN
	1    3700 4150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR057
U 1 1 52B224D5
P 7500 1550
AR Path="/52B224D5" Ref="#PWR057"  Part="1" 
AR Path="/5239FA54/52B224D5" Ref="#PWR056"  Part="1" 
F 0 "#PWR056" H 7500 1550 30  0001 C CNN
F 1 "GND" H 7500 1480 30  0001 C CNN
F 2 "" H 7500 1550 60  0001 C CNN
F 3 "" H 7500 1550 60  0001 C CNN
	1    7500 1550
	1    0    0    -1  
$EndComp
$Comp
L RAMPS-FD-rescue:R_PACK4 RP401
U 1 1 52B22857
P 3850 6950
F 0 "RP401" H 3850 7400 40  0000 C CNN
F 1 "10k" H 3850 6900 40  0000 C CNN
F 2 "Resistors_SMD:R_Cat16-4" H 3850 6950 60  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Yageo%20PDFs/YC102-358,TC122-164_Series_DS.pdf" H 3850 6950 60  0001 C CNN
F 4 "RES ARRAY 4 RES 10K OHM 1206" H 0   0   50  0001 C CNN "Description"
F 5 "Yageo" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "YC164-JR-0710KL" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "YC164J-10KCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/yageo/YC164-JR-0710KL/YC164J-10KCT-ND/1005702" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "1206" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    3850 6950
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR058
U 1 1 52B22A39
P 6300 7550
AR Path="/52B22A39" Ref="#PWR058"  Part="1" 
AR Path="/5239FA54/52B22A39" Ref="#PWR057"  Part="1" 
F 0 "#PWR057" H 6300 7550 30  0001 C CNN
F 1 "GND" H 6300 7480 30  0001 C CNN
F 2 "" H 6300 7550 60  0001 C CNN
F 3 "" H 6300 7550 60  0001 C CNN
	1    6300 7550
	1    0    0    -1  
$EndComp
Text Notes 10200 2200 0    60   ~ 0
VCC = 5V 
Text Notes 9500 2950 0    60   ~ 12
Must use ACT (or HCT) type buffers. \nInputs are compatible with \n3.3V or 5V logic
Wire Wire Line
	5350 5100 6300 5100
Connection ~ 7200 3000
Wire Wire Line
	5800 3000 7200 3000
Wire Wire Line
	7200 3000 7200 1900
Connection ~ 7000 2800
Wire Wire Line
	7000 1900 7000 2800
Connection ~ 6500 2600
Wire Wire Line
	6500 1900 6500 2600
Connection ~ 6300 2400
Wire Wire Line
	6300 1900 6300 2400
Wire Wire Line
	6200 1350 6300 1350
Connection ~ 6900 1350
Wire Wire Line
	7000 1350 7000 1500
Connection ~ 6400 1350
Wire Wire Line
	6500 1350 6500 1500
Connection ~ 6200 1350
Wire Wire Line
	6300 1500 6300 1350
Wire Wire Line
	3000 5700 3700 5700
Connection ~ 4150 5400
Wire Wire Line
	4150 6150 4850 6150
Wire Wire Line
	4150 3200 4150 3300
Connection ~ 4150 4050
Wire Wire Line
	4150 4800 4900 4800
Wire Wire Line
	3000 4350 3500 4350
Wire Wire Line
	875  3300 1550 3300
Wire Wire Line
	3150 2900 3400 2900
Wire Wire Line
	10650 1700 10650 1450
Wire Wire Line
	10100 900  10100 1150
Wire Wire Line
	2100 1600 2500 1600
Wire Wire Line
	5800 2700 6900 2700
Wire Wire Line
	5800 2500 6400 2500
Wire Wire Line
	5800 2300 6200 2300
Wire Wire Line
	3150 2800 4400 2800
Wire Wire Line
	4400 2600 3150 2600
Wire Wire Line
	3100 2400 4400 2400
Wire Wire Line
	3100 2300 4400 2300
Wire Wire Line
	3100 2500 4400 2500
Wire Wire Line
	4400 2700 3150 2700
Wire Wire Line
	5800 2400 6300 2400
Wire Wire Line
	5800 2600 6500 2600
Wire Wire Line
	5800 2800 7000 2800
Wire Wire Line
	3150 3000 3700 3000
Wire Wire Line
	1300 2350 1300 2200
Wire Wire Line
	1150 1600 1550 1600
Wire Wire Line
	5800 2900 7100 2900
Wire Wire Line
	10100 1450 10100 1700
Wire Wire Line
	10650 1150 10650 900 
Wire Wire Line
	4150 3200 4400 3200
Wire Wire Line
	3050 5100 3600 5100
Wire Wire Line
	4150 4050 4900 4050
Connection ~ 4150 3300
Wire Wire Line
	4150 5400 4900 5400
Connection ~ 4150 4800
Wire Wire Line
	2950 6450 3800 6450
Wire Wire Line
	6200 1200 6200 1350
Wire Wire Line
	6400 1350 6400 1500
Connection ~ 6300 1350
Wire Wire Line
	6900 1350 6900 1500
Connection ~ 6500 1350
Wire Wire Line
	6200 1900 6200 2300
Connection ~ 6200 2300
Wire Wire Line
	6400 1900 6400 2500
Connection ~ 6400 2500
Wire Wire Line
	6900 1900 6900 2700
Connection ~ 6900 2700
Wire Wire Line
	7100 1900 7100 2900
Connection ~ 7100 2900
Wire Wire Line
	5350 4350 6400 4350
Wire Wire Line
	1150 1800 1300 1800
Wire Wire Line
	1550 1400 1550 1600
Connection ~ 1550 1600
Wire Wire Line
	3700 7150 3700 7300
Wire Wire Line
	5300 6450 6100 6450
Wire Wire Line
	5350 5700 6200 5700
Wire Notes Line
	8400 2250 8600 2250
Wire Notes Line
	8600 2900 8600 6550
Wire Notes Line
	8600 6550 8400 6550
Wire Wire Line
	3700 3800 3700 4050
Wire Wire Line
	3400 3800 3400 4050
Wire Wire Line
	3400 4050 3700 4050
Connection ~ 3700 4050
Wire Notes Line
	8400 2800 8600 2800
Wire Notes Line
	8600 2800 8600 2250
Wire Notes Line
	8400 2900 8600 2900
Connection ~ 7200 1350
Wire Wire Line
	7100 1350 7100 1500
Wire Wire Line
	7200 1350 7200 1500
Wire Wire Line
	7100 1350 7200 1350
Wire Wire Line
	7500 1350 7500 1550
Wire Wire Line
	6300 6700 6300 5100
Connection ~ 6300 5100
Wire Wire Line
	6400 6700 6400 4350
Connection ~ 6400 4350
Wire Wire Line
	3600 7150 3600 7300
Wire Wire Line
	3500 7300 3600 7300
Connection ~ 3700 7300
Wire Wire Line
	3500 7150 3500 7300
Connection ~ 3600 7300
Wire Wire Line
	3800 7300 3800 7150
Wire Wire Line
	3600 6750 3600 5100
Connection ~ 3600 5100
Wire Wire Line
	3500 6750 3500 4350
Connection ~ 3500 4350
Wire Wire Line
	6300 7100 6300 7250
Wire Wire Line
	6200 7100 6200 7250
Wire Wire Line
	6100 7250 6200 7250
Connection ~ 6300 7250
Wire Wire Line
	6100 7100 6100 7250
Connection ~ 6200 7250
Wire Wire Line
	6400 7250 6400 7100
Wire Wire Line
	3700 3500 3700 3000
Connection ~ 3700 3000
Wire Wire Line
	3400 3500 3400 2900
Connection ~ 3400 2900
Connection ~ 3700 5700
Connection ~ 3800 6450
Wire Wire Line
	3700 6750 3700 5700
Wire Wire Line
	3800 6450 3800 6750
Connection ~ 6200 5700
Connection ~ 6100 6450
Wire Wire Line
	6100 6700 6100 6450
Wire Wire Line
	6200 6700 6200 5700
Wire Wire Line
	1550 1100 1550 1000
Wire Wire Line
	1300 1800 1300 1900
Wire Notes Line
	825  1450 825  775 
$Comp
L RAMPS-FD-rescue:LED-RESCUE-RAMPS-FD D1
U 1 1 5725588E
P 1450 4850
F 0 "D1" H 1450 4950 50  0000 C CNN
F 1 "ESTOP" H 1450 4700 50  0000 C CNN
F 2 "rmc:LED-0805_GREEN" H 1450 4850 60  0001 C CNN
F 3 "https://dammedia.osram.info/media/resource/hires/osram-dam-2493936/LG%20R971.pdf" H 1450 4850 60  0001 C CNN
F 4 "LED GREEN DIFFUSED 0805 SMD" H 0   0   50  0001 C CNN "Description"
F 5 "OSRAM Opto Semiconductors Inc." H 0   0   50  0001 C CNN "Manufacturer"
F 6 "LG R971-KN-1" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 8 "475-1410-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 9 "https://www.digikey.com/product-detail/en/osram-opto-semiconductors-inc/LG-R971-KN-1/475-1410-1-ND/1802598" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 10 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 11 "0805" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    1450 4850
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR058
U 1 1 572566C5
P 1450 5775
F 0 "#PWR058" H 1450 5775 30  0001 C CNN
F 1 "GND" H 1450 5705 30  0001 C CNN
F 2 "" H 1450 5775 60  0001 C CNN
F 3 "" H 1450 5775 60  0001 C CNN
	1    1450 5775
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 572566D2
P 1450 4425
F 0 "R3" V 1530 4425 50  0000 C CNN
F 1 "330R" V 1450 4425 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 1450 4425 60  0001 C CNN
F 3 "http://industrial.panasonic.com/www-cgi/jvcr13pz.cgi?E+PZ+3+AOA0001+ERJ3GEYJ104V+7+WW" H 1450 4425 60  0001 C CNN
F 4 "RES SMD 330 OHM 5% 1/10W 0603" H 0   0   50  0001 C CNN "Description"
F 5 "‎Panasonic Electronic Components‎" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "ERJ-3GEYJ331V" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "P330GCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/panasonic-electronic-components/ERJ-3GEYJ331V/P330GCT-ND/134778" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    1450 4425
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR059
U 1 1 572566D9
P 1450 4200
F 0 "#PWR059" H 1450 4050 50  0001 C CNN
F 1 "+5V" H 1450 4340 50  0000 C CNN
F 2 "" H 1450 4200 50  0000 C CNN
F 3 "" H 1450 4200 50  0000 C CNN
	1    1450 4200
	1    0    0    -1  
$EndComp
$Comp
L RMC:MOSFET_N Q1
U 1 1 572566DF
P 1375 5425
F 0 "Q1" H 1375 5625 60  0000 R CNN
F 1 "DMN2075" H 1375 5175 60  0000 R CNN
F 2 "libcms:SOT23GDS" H 1375 5425 60  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/DMN2075U.pdf" H 1375 5425 60  0001 C CNN
F 4 "MOSFET N-CH 20V 4.2A SOT23" H 0   0   50  0001 C CNN "Description"
F 5 "SOT-23-3" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "Diodes Incorporated" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "DMN2075U-7" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "DMN2075U-7DICT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/diodes-incorporated/DMN2075U-7/DMN2075U-7DICT-ND/2182579" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    1375 5425
	1    0    0    -1  
$EndComp
Wire Wire Line
	875  5500 1075 5500
Wire Wire Line
	1450 5700 1450 5775
Wire Wire Line
	1450 4650 1450 4575
Wire Wire Line
	1450 4200 1450 4275
Wire Wire Line
	1450 5150 1450 5050
Wire Wire Line
	875  3300 875  5500
Connection ~ 1550 3300
Wire Wire Line
	7200 3000 7600 3000
Wire Wire Line
	7000 2800 7600 2800
Wire Wire Line
	6500 2600 7600 2600
Wire Wire Line
	6300 2400 7600 2400
Wire Wire Line
	6900 1350 7000 1350
Wire Wire Line
	6400 1350 6500 1350
Wire Wire Line
	6200 1350 6200 1500
Wire Wire Line
	4150 5400 4150 6150
Wire Wire Line
	4150 4050 4150 4800
Wire Wire Line
	4150 3300 4150 4050
Wire Wire Line
	4150 3300 4400 3300
Wire Wire Line
	4150 4800 4150 5400
Wire Wire Line
	6300 1350 6400 1350
Wire Wire Line
	6500 1350 6900 1350
Wire Wire Line
	6200 2300 7600 2300
Wire Wire Line
	6400 2500 7600 2500
Wire Wire Line
	6900 2700 7600 2700
Wire Wire Line
	7100 2900 7600 2900
Wire Wire Line
	1550 1600 1700 1600
Wire Wire Line
	1550 1600 1550 3300
Wire Wire Line
	3700 4050 3700 4150
Wire Wire Line
	7200 1350 7500 1350
Wire Wire Line
	6300 5100 7650 5100
Wire Wire Line
	6400 4350 7650 4350
Wire Wire Line
	3700 7300 3700 7600
Wire Wire Line
	3700 7300 3800 7300
Wire Wire Line
	3600 7300 3700 7300
Wire Wire Line
	3600 5100 4450 5100
Wire Wire Line
	3500 4350 4450 4350
Wire Wire Line
	6300 7250 6300 7550
Wire Wire Line
	6300 7250 6400 7250
Wire Wire Line
	6200 7250 6300 7250
Wire Wire Line
	3700 3000 4400 3000
Wire Wire Line
	3400 2900 4400 2900
Wire Wire Line
	3700 5700 4450 5700
Wire Wire Line
	3800 6450 4400 6450
Wire Wire Line
	6200 5700 7600 5700
Wire Wire Line
	6100 6450 7600 6450
Wire Wire Line
	1550 3300 4150 3300
$EndSCHEMATC
