EESchema Schematic File Version 4
LIBS:RAMPS-FD-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 10 10
Title "RAMPS-FD (RAMPS for Arduino Due)"
Date "2016-07-17"
Rev "v2.2"
Comp ""
Comment1 "Derived from RAMPS 1.4 reprap.org/wiki/RAMPS1.4"
Comment2 "GPL v3"
Comment3 "Bob Cousins 2016"
Comment4 ""
$EndDescr
$Comp
L Device:R R504
U 1 1 519E9454
P 4550 1550
AR Path="/519E9454" Ref="R504"  Part="1" 
AR Path="/52C42F11/519E9454" Ref="R504"  Part="1" 
F 0 "R504" V 4450 1550 50  0000 C CNN
F 1 "4k7" V 4550 1550 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 4550 1550 60  0001 C CNN
F 3 "http://industrial.panasonic.com/www-cgi/jvcr13pz.cgi?E+PZ+3+AOA0001+ERJ3GEYJ104V+7+WW" H 4550 1550 60  0001 C CNN
F 4 "RES SMD 4.7K OHM 5% 1/10W 0603" H 0   0   50  0001 C CNN "Description"
F 5 "‎Panasonic Electronic Components‎" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "ERJ-3GEYJ472V" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "P4.7KGCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/panasonic-electronic-components/ERJ-3GEYJ472V/P4.7KGCT-ND/135199" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    4550 1550
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C504
U 1 1 519E9453
P 5500 1800
AR Path="/519E9453" Ref="C504"  Part="1" 
AR Path="/52C42F11/519E9453" Ref="C504"  Part="1" 
F 0 "C504" H 5550 1900 50  0000 L CNN
F 1 "100n" H 5550 1700 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 5500 1800 60  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/CL10F104ZA8NNNC.jsp" H 5500 1800 60  0001 C CNN
F 4 "CAP CER 0.1UF 25V Y5V 0603" H 0   0   50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "CL10F104ZA8NNNC" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "1276-1011-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/products/en/capacitors/ceramic-capacitors/60?k=&pkeyword=&pv7=2&FV=380014%2C400005%2C440003%2C1f140000%2Cmu0.1%C2%B5F%7C2049%2Cffe0003c&quantity=0&ColumnSort=0&page=1&stock=1&pageSize=25" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    5500 1800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R505
U 1 1 519E9446
P 4600 3350
AR Path="/519E9446" Ref="R505"  Part="1" 
AR Path="/52C42F11/519E9446" Ref="R505"  Part="1" 
F 0 "R505" V 4500 3350 50  0000 C CNN
F 1 "4k7" V 4600 3350 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 4600 3350 60  0001 C CNN
F 3 "http://industrial.panasonic.com/www-cgi/jvcr13pz.cgi?E+PZ+3+AOA0001+ERJ3GEYJ104V+7+WW" H 4600 3350 60  0001 C CNN
F 4 "RES SMD 4.7K OHM 5% 1/10W 0603" H 0   0   50  0001 C CNN "Description"
F 5 "‎Panasonic Electronic Components‎" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "ERJ-3GEYJ472V" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "P4.7KGCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/panasonic-electronic-components/ERJ-3GEYJ472V/P4.7KGCT-ND/135199" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    4600 3350
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C505
U 1 1 519E9445
P 5600 3600
AR Path="/519E9445" Ref="C505"  Part="1" 
AR Path="/52C42F11/519E9445" Ref="C505"  Part="1" 
F 0 "C505" H 5650 3700 50  0000 L CNN
F 1 "100n" H 5650 3500 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 5600 3600 60  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/CL10F104ZA8NNNC.jsp" H 5600 3600 60  0001 C CNN
F 4 "CAP CER 0.1UF 25V Y5V 0603" H 0   0   50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "CL10F104ZA8NNNC" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "1276-1011-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/products/en/capacitors/ceramic-capacitors/60?k=&pkeyword=&pv7=2&FV=380014%2C400005%2C440003%2C1f140000%2Cmu0.1%C2%B5F%7C2049%2Cffe0003c&quantity=0&ColumnSort=0&page=1&stock=1&pageSize=25" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    5600 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C503
U 1 1 519E9435
P 5600 5100
AR Path="/519E9435" Ref="C503"  Part="1" 
AR Path="/52C42F11/519E9435" Ref="C503"  Part="1" 
F 0 "C503" H 5650 5200 50  0000 L CNN
F 1 "100n" H 5650 5000 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 5600 5100 60  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/CL10F104ZA8NNNC.jsp" H 5600 5100 60  0001 C CNN
F 4 "CAP CER 0.1UF 25V Y5V 0603" H 0   0   50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "CL10F104ZA8NNNC" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "1276-1011-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/products/en/capacitors/ceramic-capacitors/60?k=&pkeyword=&pv7=2&FV=380014%2C400005%2C440003%2C1f140000%2Cmu0.1%C2%B5F%7C2049%2Cffe0003c&quantity=0&ColumnSort=0&page=1&stock=1&pageSize=25" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    5600 5100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R503
U 1 1 519E9434
P 4550 4850
AR Path="/519E9434" Ref="R503"  Part="1" 
AR Path="/52C42F11/519E9434" Ref="R503"  Part="1" 
F 0 "R503" V 4450 4850 50  0000 C CNN
F 1 "4k7" V 4550 4850 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 4550 4850 60  0001 C CNN
F 3 "http://industrial.panasonic.com/www-cgi/jvcr13pz.cgi?E+PZ+3+AOA0001+ERJ3GEYJ104V+7+WW" H 4550 4850 60  0001 C CNN
F 4 "RES SMD 4.7K OHM 5% 1/10W 0603" H 0   0   50  0001 C CNN "Description"
F 5 "‎Panasonic Electronic Components‎" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "ERJ-3GEYJ472V" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "P4.7KGCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/panasonic-electronic-components/ERJ-3GEYJ472V/P4.7KGCT-ND/135199" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    4550 4850
	0    -1   -1   0   
$EndComp
Text Notes 6950 1200 0    50   ~ 0
To CPU
Text Notes 2200 2100 0    50   ~ 0
From \nthermistors
$Comp
L Device:R R502
U 1 1 517BADA5
P 4500 6400
AR Path="/517BADA5" Ref="R502"  Part="1" 
AR Path="/52C42F11/517BADA5" Ref="R502"  Part="1" 
F 0 "R502" V 4400 6400 50  0000 C CNN
F 1 "4k7" V 4500 6400 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 4500 6400 60  0001 C CNN
F 3 "http://industrial.panasonic.com/www-cgi/jvcr13pz.cgi?E+PZ+3+AOA0001+ERJ3GEYJ104V+7+WW" H 4500 6400 60  0001 C CNN
F 4 "RES SMD 4.7K OHM 5% 1/10W 0603" H 0   0   50  0001 C CNN "Description"
F 5 "‎Panasonic Electronic Components‎" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "ERJ-3GEYJ472V" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "P4.7KGCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/panasonic-electronic-components/ERJ-3GEYJ472V/P4.7KGCT-ND/135199" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    4500 6400
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C502
U 1 1 517BAD21
P 5650 6700
AR Path="/517BAD21" Ref="C502"  Part="1" 
AR Path="/52C42F11/517BAD21" Ref="C502"  Part="1" 
F 0 "C502" H 5700 6800 50  0000 L CNN
F 1 "100n" H 5700 6600 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 5650 6700 60  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/CL10F104ZA8NNNC.jsp" H 5650 6700 60  0001 C CNN
F 4 "CAP CER 0.1UF 25V Y5V 0603" H 0   0   50  0001 C CNN "Description"
F 5 "Samsung Electro-Mechanics" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "CL10F104ZA8NNNC" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "1276-1011-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/products/en/capacitors/ceramic-capacitors/60?k=&pkeyword=&pv7=2&FV=380014%2C400005%2C440003%2C1f140000%2Cmu0.1%C2%B5F%7C2049%2Cffe0003c&quantity=0&ColumnSort=0&page=1&stock=1&pageSize=25" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    5650 6700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R501
U 1 1 5178644E
P 4000 6050
AR Path="/5178644E" Ref="R501"  Part="1" 
AR Path="/52C42F11/5178644E" Ref="R501"  Part="1" 
F 0 "R501" V 4080 6050 50  0000 C CNN
F 1 "1k" V 4000 6050 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 4000 6050 60  0001 C CNN
F 3 "http://industrial.panasonic.com/www-cgi/jvcr13pz.cgi?E+PZ+3+AOA0001+ERJ3GEYJ104V+7+WW" H 4000 6050 60  0001 C CNN
F 4 "RES SMD 1K OHM 5% 1/10W 0603" H 0   0   50  0001 C CNN "Description"
F 5 "‎Panasonic Electronic Components‎" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "ERJ-3GEYJ102V" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "P1.0KGCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/panasonic-electronic-components/ERJ-3GEYJ102V/P1.0KGCT-ND/134874" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    4000 6050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C501
U 1 1 5178644D
P 6100 6700
AR Path="/5178644D" Ref="C501"  Part="1" 
AR Path="/52C42F11/5178644D" Ref="C501"  Part="1" 
F 0 "C501" H 6150 6800 50  0000 L CNN
F 1 "10u" H 6150 6600 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 6100 6700 60  0001 C CNN
F 3 "https://product.tdk.com/info/en/catalog/datasheets/mlcc_commercial_general_en.pdf" H 6100 6700 60  0001 C CNN
F 4 "CAP CER 10UF 16V X5R 0603" H 0   0   50  0001 C CNN "Description"
F 5 "TDK Corporation" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "C1608X5R1C106M080AB" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "445-9065-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/tdk-corporation/C1608X5R1C106M080AB/445-9065-1-ND/3648729" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    6100 6700
	1    0    0    -1  
$EndComp
Text GLabel 6550 6400 2    50   Output ~ 0
THERM3
$Comp
L RAMPS-FD-rescue:CONN_4X2 P301
U 1 1 517863D5
P 2350 2550
F 0 "P301" H 2350 2800 50  0000 C CNN
F 1 "THERMISTORS" V 2350 2550 40  0000 C CNN
F 2 "w_pin_strip:pin_strip_4x2" H 2350 2550 60  0001 C CNN
F 3 "https://media.digikey.com/PDF/Data%20Sheets/Sullins%20PDFs/xRxCzzzSxxN-RC_ST_11635-B.pdf" H 2350 2550 60  0001 C CNN
F 4 "CONN HEADER .100\" SNGL STR 4POS" H 0   0   50  0001 C CNN "Description"
F 5 "Sullins Connector Solutions" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "PRPC004SAAN-RC" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "2" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "S1011EC-04-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/products/en?keywords=S1011EC-04-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    2350 2550
	1    0    0    -1  
$EndComp
Text Notes 3300 750  2    80   ~ 16
Thermistor Inputs
Text GLabel 6500 4850 2    50   Output ~ 0
THERM2
$Comp
L Device:C C303
U 1 1 50FC3AB4
P 6100 5100
AR Path="/50FC3AB4" Ref="C303"  Part="1" 
AR Path="/52C42F11/50FC3AB4" Ref="C303"  Part="1" 
F 0 "C303" H 6150 5200 50  0000 L CNN
F 1 "10u" H 6150 5000 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 6100 5100 60  0001 C CNN
F 3 "https://product.tdk.com/info/en/catalog/datasheets/mlcc_commercial_general_en.pdf" H 6100 5100 60  0001 C CNN
F 4 "CAP CER 10UF 16V X5R 0603" H 0   0   50  0001 C CNN "Description"
F 5 "TDK Corporation" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "C1608X5R1C106M080AB" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "445-9065-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/tdk-corporation/C1608X5R1C106M080AB/445-9065-1-ND/3648729" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    6100 5100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R303
U 1 1 50FC3AB3
P 4000 4550
AR Path="/50FC3AB3" Ref="R303"  Part="1" 
AR Path="/52C42F11/50FC3AB3" Ref="R303"  Part="1" 
F 0 "R303" V 4080 4550 50  0000 C CNN
F 1 "1k" V 4000 4550 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 4000 4550 60  0001 C CNN
F 3 "http://industrial.panasonic.com/www-cgi/jvcr13pz.cgi?E+PZ+3+AOA0001+ERJ3GEYJ104V+7+WW" H 4000 4550 60  0001 C CNN
F 4 "RES SMD 1K OHM 5% 1/10W 0603" H 0   0   50  0001 C CNN "Description"
F 5 "‎Panasonic Electronic Components‎" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "ERJ-3GEYJ102V" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "P1.0KGCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/panasonic-electronic-components/ERJ-3GEYJ102V/P1.0KGCT-ND/134874" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    4000 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R302
U 1 1 50FC3A9E
P 4000 3050
AR Path="/50FC3A9E" Ref="R302"  Part="1" 
AR Path="/52C42F11/50FC3A9E" Ref="R302"  Part="1" 
F 0 "R302" V 4080 3050 50  0000 C CNN
F 1 "1k" V 4000 3050 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 4000 3050 60  0001 C CNN
F 3 "http://industrial.panasonic.com/www-cgi/jvcr13pz.cgi?E+PZ+3+AOA0001+ERJ3GEYJ104V+7+WW" H 4000 3050 60  0001 C CNN
F 4 "RES SMD 1K OHM 5% 1/10W 0603" H 0   0   50  0001 C CNN "Description"
F 5 "‎Panasonic Electronic Components‎" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "ERJ-3GEYJ102V" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "P1.0KGCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/panasonic-electronic-components/ERJ-3GEYJ102V/P1.0KGCT-ND/134874" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    4000 3050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C302
U 1 1 50FC3A9D
P 6100 3600
AR Path="/50FC3A9D" Ref="C302"  Part="1" 
AR Path="/52C42F11/50FC3A9D" Ref="C302"  Part="1" 
F 0 "C302" H 6150 3700 50  0000 L CNN
F 1 "10u" H 6150 3500 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 6100 3600 60  0001 C CNN
F 3 "https://product.tdk.com/info/en/catalog/datasheets/mlcc_commercial_general_en.pdf" H 6100 3600 60  0001 C CNN
F 4 "CAP CER 10UF 16V X5R 0603" H 0   0   50  0001 C CNN "Description"
F 5 "TDK Corporation" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "C1608X5R1C106M080AB" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "445-9065-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/tdk-corporation/C1608X5R1C106M080AB/445-9065-1-ND/3648729" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    6100 3600
	1    0    0    -1  
$EndComp
Text GLabel 6400 3350 2    50   Output ~ 0
THERM1
Text GLabel 6450 1550 2    50   Output ~ 0
THERM0
$Comp
L Device:C C301
U 1 1 50FC3A14
P 6100 1800
AR Path="/50FC3A14" Ref="C301"  Part="1" 
AR Path="/52C42F11/50FC3A14" Ref="C301"  Part="1" 
F 0 "C301" H 6150 1900 50  0000 L CNN
F 1 "10u" H 6150 1700 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 6100 1800 60  0001 C CNN
F 3 "https://product.tdk.com/info/en/catalog/datasheets/mlcc_commercial_general_en.pdf" H 6100 1800 60  0001 C CNN
F 4 "CAP CER 10UF 16V X5R 0603" H 0   0   50  0001 C CNN "Description"
F 5 "TDK Corporation" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "C1608X5R1C106M080AB" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "445-9065-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/tdk-corporation/C1608X5R1C106M080AB/445-9065-1-ND/3648729" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    6100 1800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R301
U 1 1 50FC3A00
P 3900 1250
AR Path="/50FC3A00" Ref="R301"  Part="1" 
AR Path="/52C42F11/50FC3A00" Ref="R301"  Part="1" 
F 0 "R301" V 3980 1250 50  0000 C CNN
F 1 "1k" V 3900 1250 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 3900 1250 60  0001 C CNN
F 3 "http://industrial.panasonic.com/www-cgi/jvcr13pz.cgi?E+PZ+3+AOA0001+ERJ3GEYJ104V+7+WW" H 3900 1250 60  0001 C CNN
F 4 "RES SMD 1K OHM 5% 1/10W 0603" H 0   0   50  0001 C CNN "Description"
F 5 "‎Panasonic Electronic Components‎" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "ERJ-3GEYJ102V" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "P1.0KGCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/panasonic-electronic-components/ERJ-3GEYJ102V/P1.0KGCT-ND/134874" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    3900 1250
	1    0    0    -1  
$EndComp
$Comp
L power:+12P #PWR0116
U 1 1 52C4371F
P 8600 1950
F 0 "#PWR0116" H 8600 1800 50  0001 C CNN
F 1 "+12P" H 8600 2090 50  0000 C CNN
F 2 "" H 8600 1950 50  0000 C CNN
F 3 "" H 8600 1950 50  0000 C CNN
	1    8600 1950
	1    0    0    -1  
$EndComp
$Comp
L RAMPS-FD-rescue:DIODE D1002
U 1 1 52C438B2
P 5050 1800
F 0 "D1002" H 5050 1900 40  0000 C CNN
F 1 "1N4148" H 5050 1700 40  0000 C CNN
F 2 "w_smd_diode:SOD323" H 5050 1800 60  0001 C CNN
F 3 "http://www.mccsemi.com/up_pdf/1N4148WX(SOD323).pdf" H 5050 1800 60  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Characteristics"
F 5 "SOD-323" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "Micro Commercial Co" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "1N4148WX-TP" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "1N4148WXTPMSCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/micro-commercial-co/1N4148WX-TP/1N4148WXTPMSCT-ND/717312" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "DIODE GEN PURP 75V 150MA SOD323" H 0   0   50  0001 C CNN "Description"
F 12 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5050 1800
	0    -1   -1   0   
$EndComp
$Comp
L RAMPS-FD-rescue:DIODE D1001
U 1 1 52C43922
P 5050 1300
F 0 "D1001" H 5050 1400 40  0000 C CNN
F 1 "1N4148" H 5050 1200 40  0000 C CNN
F 2 "w_smd_diode:SOD323" H 5050 1300 60  0001 C CNN
F 3 "http://www.mccsemi.com/up_pdf/1N4148WX(SOD323).pdf" H 5050 1300 60  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Characteristics"
F 5 "SOD-323" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "Micro Commercial Co" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "1N4148WX-TP" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "1N4148WXTPMSCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/micro-commercial-co/1N4148WX-TP/1N4148WXTPMSCT-ND/717312" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "DIODE GEN PURP 75V 150MA SOD323" H 0   0   50  0001 C CNN "Description"
F 12 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5050 1300
	0    -1   -1   0   
$EndComp
Text GLabel 5350 1000 2    60   Output ~ 0
OV_REF
$Comp
L Device:R R1002
U 1 1 52C441CF
P 8600 2450
AR Path="/52C441CF" Ref="R1002"  Part="1" 
AR Path="/52C42F11/52C441CF" Ref="R1002"  Part="1" 
F 0 "R1002" V 8680 2450 40  0000 C CNN
F 1 "6k8" V 8607 2451 40  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 8530 2450 30  0001 C CNN
F 3 "http://industrial.panasonic.com/www-cgi/jvcr13pz.cgi?E+PZ+3+AOA0001+ERJ3GEYJ104V+7+WW" H 8600 2450 30  0001 C CNN
F 4 "RES SMD 6.8K OHM 5% 1/10W 0603" H 0   0   50  0001 C CNN "Description"
F 5 "‎Panasonic Electronic Components‎" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "ERJ-3GEYJ682V" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "P6.8KGCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/panasonic-electronic-components/ERJ-3GEYJ682V/P6.8KGCT-ND/135260" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    8600 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1004
U 1 1 52C441EB
P 9200 3700
AR Path="/52C441EB" Ref="R1004"  Part="1" 
AR Path="/52C42F11/52C441EB" Ref="R1004"  Part="1" 
F 0 "R1004" V 9280 3700 40  0000 C CNN
F 1 "1k" V 9207 3701 40  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 9130 3700 30  0001 C CNN
F 3 "http://industrial.panasonic.com/www-cgi/jvcr13pz.cgi?E+PZ+3+AOA0001+ERJ3GEYJ104V+7+WW" H 9200 3700 30  0001 C CNN
F 4 "RES SMD 1K OHM 5% 1/10W 0603" H 0   0   50  0001 C CNN "Description"
F 5 "‎Panasonic Electronic Components‎" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "ERJ-3GEYJ102V" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "P1.0KGCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/panasonic-electronic-components/ERJ-3GEYJ102V/P1.0KGCT-ND/134874" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    9200 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1003
U 1 1 52C4448C
P 9200 3100
AR Path="/52C4448C" Ref="R1003"  Part="1" 
AR Path="/52C42F11/52C4448C" Ref="R1003"  Part="1" 
F 0 "R1003" V 9280 3100 40  0000 C CNN
F 1 "1k8" V 9207 3101 40  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 9130 3100 30  0001 C CNN
F 3 "http://industrial.panasonic.com/www-cgi/jvcr13pz.cgi?E+PZ+3+AOA0001+ERJ3GEYJ104V+7+WW" H 9200 3100 30  0001 C CNN
F 4 "RES SMD 1.8K OHM 5% 1/10W 0603" H 0   0   50  0001 C CNN "Description"
F 5 "‎Panasonic Electronic Components‎" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "ERJ-3GEYJ182V" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "P1.8KGCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/panasonic-electronic-components/ERJ-3GEYJ182V/P1.8KGCT-ND/135040" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
	1    9200 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C1001
U 1 1 52C4464E
P 9600 3400
AR Path="/52C4464E" Ref="C1001"  Part="1" 
AR Path="/52C42F11/52C4464E" Ref="C1001"  Part="1" 
F 0 "C1001" H 9650 3500 50  0000 L CNN
F 1 "10u" H 9650 3300 50  0000 L CNN
F 2 "Capacitors_SMD:c_elec_4x5.3" H 9600 3400 60  0001 C CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDE0000/ABA0000C1145.pdf" H 9600 3400 60  0001 C CNN
F 4 "CAP ALUM 10UF 20% 16V SMD" H 0   0   50  0001 C CNN "Description"
F 5 "‎Panasonic Electronic Components‎" H 0   0   50  0001 C CNN "Manufacturer"
F 6 "EEE-1CA100SR" H 0   0   50  0001 C CNN "Mfr PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "PCE3878CT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/panasonic-electronic-components/EEE-1CA100SR/PCE3878CT-ND/766254" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    9600 3400
	1    0    0    -1  
$EndComp
Text Notes 9950 3000 0    60   ~ 0
Limit at about 3.1V
Text Notes 7950 3700 0    50   ~ 0
Vref = 1.24V
Text GLabel 9850 2800 2    60   Input ~ 0
OV_REF
$Comp
L RAMPS-FD-rescue:DIODE D1007
U 1 1 52C565E9
P 5100 3100
F 0 "D1007" H 5100 3200 40  0000 C CNN
F 1 "1N4148" H 5100 3000 40  0000 C CNN
F 2 "w_smd_diode:SOD323" H 5100 3100 60  0001 C CNN
F 3 "http://www.mccsemi.com/up_pdf/1N4148WX(SOD323).pdf" H 5100 3100 60  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Characteristics"
F 5 "SOD-323" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "Micro Commercial Co" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "1N4148WX-TP" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "1N4148WXTPMSCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/micro-commercial-co/1N4148WX-TP/1N4148WXTPMSCT-ND/717312" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "DIODE GEN PURP 75V 150MA SOD323" H 0   0   50  0001 C CNN "Description"
F 12 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5100 3100
	0    -1   -1   0   
$EndComp
Text GLabel 5400 2700 2    60   Output ~ 0
OV_REF
$Comp
L RAMPS-FD-rescue:DIODE D1008
U 1 1 52C565F2
P 5100 3600
F 0 "D1008" H 5100 3700 40  0000 C CNN
F 1 "1N4148" H 5100 3500 40  0000 C CNN
F 2 "w_smd_diode:SOD323" H 5100 3600 60  0001 C CNN
F 3 "http://www.mccsemi.com/up_pdf/1N4148WX(SOD323).pdf" H 5100 3600 60  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Characteristics"
F 5 "SOD-323" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "Micro Commercial Co" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "1N4148WX-TP" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "1N4148WXTPMSCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/micro-commercial-co/1N4148WX-TP/1N4148WXTPMSCT-ND/717312" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "DIODE GEN PURP 75V 150MA SOD323" H 0   0   50  0001 C CNN "Description"
F 12 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5100 3600
	0    -1   -1   0   
$EndComp
$Comp
L RAMPS-FD-rescue:DIODE D1004
U 1 1 52C565F8
P 5000 5100
F 0 "D1004" H 5000 5200 40  0000 C CNN
F 1 "1N4148" H 5000 5000 40  0000 C CNN
F 2 "w_smd_diode:SOD323" H 5000 5100 60  0001 C CNN
F 3 "http://www.mccsemi.com/up_pdf/1N4148WX(SOD323).pdf" H 5000 5100 60  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Characteristics"
F 5 "SOD-323" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "Micro Commercial Co" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "1N4148WX-TP" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "1N4148WXTPMSCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/micro-commercial-co/1N4148WX-TP/1N4148WXTPMSCT-ND/717312" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "DIODE GEN PURP 75V 150MA SOD323" H 0   0   50  0001 C CNN "Description"
F 12 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5000 5100
	0    -1   -1   0   
$EndComp
$Comp
L RAMPS-FD-rescue:DIODE D1003
U 1 1 52C56608
P 5000 4550
F 0 "D1003" H 5000 4650 40  0000 C CNN
F 1 "1N4148" H 5000 4450 40  0000 C CNN
F 2 "w_smd_diode:SOD323" H 5000 4550 60  0001 C CNN
F 3 "http://www.mccsemi.com/up_pdf/1N4148WX(SOD323).pdf" H 5000 4550 60  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Characteristics"
F 5 "SOD-323" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "Micro Commercial Co" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "1N4148WX-TP" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "1N4148WXTPMSCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/micro-commercial-co/1N4148WX-TP/1N4148WXTPMSCT-ND/717312" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "DIODE GEN PURP 75V 150MA SOD323" H 0   0   50  0001 C CNN "Description"
F 12 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5000 4550
	0    -1   -1   0   
$EndComp
Text GLabel 5300 4250 2    60   Output ~ 0
OV_REF
$Comp
L RAMPS-FD-rescue:DIODE D1005
U 1 1 52C56611
P 5000 6100
F 0 "D1005" H 5000 6200 40  0000 C CNN
F 1 "1N4148" H 5000 6000 40  0000 C CNN
F 2 "w_smd_diode:SOD323" H 5000 6100 60  0001 C CNN
F 3 "http://www.mccsemi.com/up_pdf/1N4148WX(SOD323).pdf" H 5000 6100 60  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Characteristics"
F 5 "SOD-323" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "Micro Commercial Co" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "1N4148WX-TP" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "1N4148WXTPMSCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/micro-commercial-co/1N4148WX-TP/1N4148WXTPMSCT-ND/717312" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "DIODE GEN PURP 75V 150MA SOD323" H 0   0   50  0001 C CNN "Description"
F 12 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5000 6100
	0    -1   -1   0   
$EndComp
Text GLabel 5300 5750 2    60   Output ~ 0
OV_REF
$Comp
L RAMPS-FD-rescue:DIODE D1006
U 1 1 52C5661A
P 5000 6700
F 0 "D1006" H 5000 6800 40  0000 C CNN
F 1 "1N4148" H 5000 6600 40  0000 C CNN
F 2 "w_smd_diode:SOD323" H 5000 6700 60  0001 C CNN
F 3 "http://www.mccsemi.com/up_pdf/1N4148WX(SOD323).pdf" H 5000 6700 60  0001 C CNN
F 4 "" H 0   0   50  0001 C CNN "Characteristics"
F 5 "SOD-323" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "Micro Commercial Co" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "1N4148WX-TP" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "1N4148WXTPMSCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/product-detail/en/micro-commercial-co/1N4148WX-TP/1N4148WXTPMSCT-ND/717312" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "DIODE GEN PURP 75V 150MA SOD323" H 0   0   50  0001 C CNN "Description"
F 12 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5000 6700
	0    -1   -1   0   
$EndComp
$Comp
L RMC:TL431_SOT23_RMC U1001
U 1 1 52C570FC
P 8600 3400
F 0 "U1001" H 8750 3500 40  0000 L BNN
F 1 "TL431_SOT23_RMC" H 8750 3350 40  0000 L TNN
F 2 "libcms:SOT23" H 8750 3250 30  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/lmv431.pdf" H 8600 3400 60  0001 C CNN
F 4 "IC VREF SHUNT ADJ SOT23-3" H 0   0   50  0001 C CNN "Description"
F 5 "SOT-23-3" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "Texas Instruments" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "LMV431AIMF/NOPB" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 9 "LMV431AIMF/NOPBCT-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 10 "https://www.digikey.com/products/en?keywords=LMV431AIMF%2FNOPBCT-ND" H 0   0   50  0001 C CNN "Vendor 1 URL"
F 11 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    8600 3400
	-1   0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR0121
U 1 1 52CCBFF0
P 1750 3000
AR Path="/52CCBFF0" Ref="#PWR0121"  Part="1" 
AR Path="/52C42F11/52CCBFF0" Ref="#PWR0117"  Part="1" 
F 0 "#PWR0117" H 1750 2750 50  0001 C CNN
F 1 "GNDA" H 1750 2850 50  0000 C CNN
F 2 "" H 1750 3000 50  0000 C CNN
F 3 "" H 1750 3000 50  0000 C CNN
	1    1750 3000
	1    0    0    -1  
$EndComp
$Comp
L RMC:+V_LOGIC #PWR0118
U 1 1 537E81B8
P 1150 1025
F 0 "#PWR0118" H 1150 995 30  0001 C CNN
F 1 "+V_LOGIC" H 1150 1125 30  0000 C CNN
F 2 "" H 1150 1025 60  0001 C CNN
F 3 "" H 1150 1025 60  0001 C CNN
	1    1150 1025
	1    0    0    -1  
$EndComp
$Comp
L RAMPS-FD-rescue:INDUCTOR_SMALL L1
U 1 1 5723B595
P 1625 1175
F 0 "L1" H 1625 1275 50  0000 C CNN
F 1 "FB" H 1625 1125 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 1625 1175 50  0001 C CNN
F 3 "https://www.murata.com/en-us/products/productdata/8796738650142/ENFA0003.pdf" H 1625 1175 50  0001 C CNN
F 4 "FERRITE BEAD 120 OHM 0603 1LN" H 0   0   50  0001 C CNN "Description"
F 5 "0603" H 0   0   50  0001 C CNN "JEDEC Pkg"
F 6 "Murata Electronics North America" H 0   0   50  0001 C CNN "Manufacturer"
F 7 "BLM18AG121SN1D" H 0   0   50  0001 C CNN "Mfr PN"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "y" H 0   0   50  0001 C CNN "Subs OK?"
F 10 "490-1011-1-ND" H 0   0   50  0001 C CNN "Vendor 1 PN"
F 11 "https://www.digikey.com/product-detail/en/murata-electronics-north-america/BLM18AG121SN1D/490-1011-1-ND/584459" H 0   0   50  0001 C CNN "Vendor 1 URL"
	1    1625 1175
	1    0    0    -1  
$EndComp
Text GLabel 2125 1175 2    60   Output ~ 0
AREF
Text GLabel 3600 950  0    60   Input ~ 0
AREF
Text GLabel 3775 2600 0    60   Input ~ 0
AREF
Text GLabel 3825 4225 0    60   Input ~ 0
AREF
Text GLabel 3800 5600 0    60   Input ~ 0
AREF
Wire Wire Line
	2750 2600 3250 2600
Wire Wire Line
	3250 2600 3250 4850
Wire Wire Line
	3250 4850 4000 4850
Wire Wire Line
	3100 1550 3900 1550
Wire Wire Line
	3100 1550 3100 2400
Wire Wire Line
	3100 2400 2750 2400
Connection ~ 5600 3350
Connection ~ 6100 2050
Wire Wire Line
	5500 1950 5500 2050
Wire Wire Line
	5050 2050 5500 2050
Wire Wire Line
	4700 1550 5050 1550
Wire Wire Line
	5600 4850 5600 4950
Wire Wire Line
	3100 6400 4000 6400
Wire Wire Line
	3100 2700 3100 6400
Wire Wire Line
	5650 6400 5650 6550
Wire Wire Line
	5650 6850 5650 7000
Connection ~ 6100 7000
Wire Wire Line
	6100 6850 6100 7000
Connection ~ 6100 6400
Wire Wire Line
	4650 6400 5000 6400
Wire Wire Line
	6100 6400 6100 6550
Wire Wire Line
	4000 5600 4000 5900
Connection ~ 1750 2600
Wire Wire Line
	1950 2500 1750 2500
Wire Wire Line
	1750 2700 1950 2700
Wire Wire Line
	3900 950  3900 1100
Wire Wire Line
	6100 1950 6100 2050
Wire Wire Line
	6100 3750 6100 3850
Connection ~ 6100 3350
Wire Wire Line
	4000 2600 4000 2900
Wire Wire Line
	4000 4225 4000 4400
Wire Wire Line
	4700 4850 5000 4850
Wire Wire Line
	6100 4850 6100 4950
Connection ~ 6100 4850
Wire Wire Line
	6100 5250 6100 5350
Wire Wire Line
	1750 2600 1950 2600
Connection ~ 1750 2700
Wire Wire Line
	1950 2400 1750 2400
Wire Wire Line
	1750 2400 1750 2500
Connection ~ 1750 2500
Wire Wire Line
	3100 2700 2750 2700
Connection ~ 5650 6400
Wire Wire Line
	5000 7000 5650 7000
Wire Wire Line
	5000 5350 5600 5350
Wire Wire Line
	5600 5350 5600 5250
Connection ~ 6100 5350
Connection ~ 5600 4850
Wire Wire Line
	5500 1650 5500 1550
Connection ~ 5500 1550
Wire Wire Line
	5600 3350 5600 3450
Wire Wire Line
	5600 3750 5600 3850
Wire Wire Line
	5100 3850 5600 3850
Connection ~ 6100 3850
Wire Wire Line
	2750 2500 3400 2500
Wire Wire Line
	3400 2500 3400 3350
Wire Wire Line
	3400 3350 4000 3350
Wire Wire Line
	8600 1950 8600 2300
Wire Wire Line
	5050 2050 5050 2000
Connection ~ 5500 2050
Wire Wire Line
	5050 1500 5050 1550
Connection ~ 5050 1550
Wire Wire Line
	6100 1650 6100 1550
Connection ~ 6100 1550
Wire Wire Line
	5350 1000 5050 1000
Wire Wire Line
	5050 1000 5050 1100
Wire Wire Line
	3900 1400 3900 1550
Connection ~ 3900 1550
Wire Wire Line
	8600 2800 9200 2800
Wire Wire Line
	8600 2600 8600 2800
Connection ~ 8600 2800
Wire Wire Line
	9200 3850 9200 4050
Wire Wire Line
	8600 3650 8600 4050
Wire Wire Line
	8600 4050 9200 4050
Connection ~ 9200 4050
Wire Wire Line
	8850 3400 9200 3400
Wire Wire Line
	9200 3250 9200 3400
Connection ~ 9200 3400
Wire Wire Line
	9200 2800 9200 2950
Connection ~ 9200 2800
Wire Wire Line
	9600 3250 9600 2800
Connection ~ 9600 2800
Wire Wire Line
	9600 4050 9600 3550
Wire Wire Line
	5400 2700 5100 2700
Wire Wire Line
	5100 2700 5100 2900
Wire Wire Line
	5300 4250 5000 4250
Wire Wire Line
	5000 4250 5000 4350
Wire Wire Line
	5300 5750 5000 5750
Wire Wire Line
	5000 5750 5000 5900
Wire Wire Line
	5100 3850 5100 3800
Connection ~ 5600 3850
Wire Wire Line
	5100 3300 5100 3350
Connection ~ 5100 3350
Wire Wire Line
	6100 3350 6100 3450
Wire Wire Line
	4000 3200 4000 3350
Connection ~ 4000 3350
Wire Wire Line
	4000 4700 4000 4850
Connection ~ 4000 4850
Wire Wire Line
	5000 4750 5000 4850
Connection ~ 5000 4850
Wire Wire Line
	5000 5350 5000 5300
Connection ~ 5600 5350
Wire Wire Line
	4000 6200 4000 6400
Connection ~ 4000 6400
Wire Wire Line
	5000 6300 5000 6400
Connection ~ 5000 6400
Wire Wire Line
	5000 7000 5000 6900
Connection ~ 5650 7000
Wire Wire Line
	4750 3350 5100 3350
Wire Wire Line
	2125 1175 1875 1175
Wire Wire Line
	1375 1175 1150 1175
Wire Wire Line
	1150 1175 1150 1025
Wire Wire Line
	3600 950  3900 950 
Wire Wire Line
	4000 5600 3800 5600
Wire Wire Line
	4000 4225 3825 4225
Wire Wire Line
	4000 2600 3775 2600
$Comp
L power:GNDA #PWR0119
U 1 1 5723EA8A
P 6100 2100
F 0 "#PWR0119" H 6100 1850 50  0001 C CNN
F 1 "GNDA" H 6100 1950 50  0000 C CNN
F 2 "" H 6100 2100 50  0000 C CNN
F 3 "" H 6100 2100 50  0000 C CNN
	1    6100 2100
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR0120
U 1 1 5723F10F
P 6100 3900
F 0 "#PWR0120" H 6100 3650 50  0001 C CNN
F 1 "GNDA" H 6100 3750 50  0000 C CNN
F 2 "" H 6100 3900 50  0000 C CNN
F 3 "" H 6100 3900 50  0000 C CNN
	1    6100 3900
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR0121
U 1 1 5723F49D
P 6100 5400
F 0 "#PWR0121" H 6100 5150 50  0001 C CNN
F 1 "GNDA" H 6100 5250 50  0000 C CNN
F 2 "" H 6100 5400 50  0000 C CNN
F 3 "" H 6100 5400 50  0000 C CNN
	1    6100 5400
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR0122
U 1 1 5723F505
P 6100 7150
F 0 "#PWR0122" H 6100 6900 50  0001 C CNN
F 1 "GNDA" H 6100 7000 50  0000 C CNN
F 2 "" H 6100 7150 50  0000 C CNN
F 3 "" H 6100 7150 50  0000 C CNN
	1    6100 7150
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR0123
U 1 1 5723F5E9
P 9200 4200
F 0 "#PWR0123" H 9200 3950 50  0001 C CNN
F 1 "GNDA" H 9200 4050 50  0000 C CNN
F 2 "" H 9200 4200 50  0000 C CNN
F 3 "" H 9200 4200 50  0000 C CNN
	1    9200 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 3350 6100 3350
Wire Wire Line
	6100 2050 6100 2100
Wire Wire Line
	6100 7000 6100 7150
Wire Wire Line
	6100 6400 6550 6400
Wire Wire Line
	1750 2600 1750 2700
Wire Wire Line
	6100 3350 6400 3350
Wire Wire Line
	6100 4850 6500 4850
Wire Wire Line
	1750 2700 1750 3000
Wire Wire Line
	1750 2500 1750 2600
Wire Wire Line
	5650 6400 6100 6400
Wire Wire Line
	6100 5350 6100 5400
Wire Wire Line
	5600 4850 6100 4850
Wire Wire Line
	5500 1550 6100 1550
Wire Wire Line
	6100 3850 6100 3900
Wire Wire Line
	5500 2050 6100 2050
Wire Wire Line
	5050 1550 5500 1550
Wire Wire Line
	5050 1550 5050 1600
Wire Wire Line
	6100 1550 6450 1550
Wire Wire Line
	3900 1550 4400 1550
Wire Wire Line
	8600 2800 8600 3150
Wire Wire Line
	9200 4050 9200 4200
Wire Wire Line
	9200 4050 9600 4050
Wire Wire Line
	9200 3400 9200 3550
Wire Wire Line
	9200 2800 9600 2800
Wire Wire Line
	9600 2800 9850 2800
Wire Wire Line
	5600 3850 6100 3850
Wire Wire Line
	5100 3350 5100 3400
Wire Wire Line
	5100 3350 5600 3350
Wire Wire Line
	4000 3350 4450 3350
Wire Wire Line
	4000 4850 4400 4850
Wire Wire Line
	5000 4850 5600 4850
Wire Wire Line
	5000 4850 5000 4900
Wire Wire Line
	5600 5350 6100 5350
Wire Wire Line
	4000 6400 4350 6400
Wire Wire Line
	5000 6400 5650 6400
Wire Wire Line
	5000 6400 5000 6500
Wire Wire Line
	5650 7000 6100 7000
$EndSCHEMATC
