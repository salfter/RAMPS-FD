# RAMPS-FD

RAMPS for Due.

I have built the board as configured here and verified that it works.  All
components are annotated for BOM generation, though it still takes a bit of
cleanup to get a usable BOM that you can hand off to a parts vendor or to
Octopart.  (As of 14 August 2018, the BOM is 100% orderable at DigiKey, at
least.)
